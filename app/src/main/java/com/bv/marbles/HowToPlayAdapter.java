package com.bv.marbles;

/**
 * Created by admin1 on 28/7/17.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class HowToPlayAdapter extends PagerAdapter {

    private ArrayList<Integer> images;
    private ArrayList<String> hint_images;
    private LayoutInflater inflater;
    private Context context;

    public HowToPlayAdapter(Context context, ArrayList<Integer> images,ArrayList<String> hint_images) {
        this.context = context;
        this.images=images;
        this.hint_images=hint_images;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.slide, view, false);
        ImageView myImage = (ImageView) myImageLayout
                .findViewById(R.id.image);
        TextView hint = (TextView)myImageLayout.findViewById(R.id.txt_hints);
        myImage.setImageResource(images.get(position));
        hint.setText(hint_images.get(position));
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}