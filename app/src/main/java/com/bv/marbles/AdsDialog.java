package com.bv.marbles;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by admin1 on 2/8/17.
 */

public class AdsDialog extends Dialog{
    TextView ads_ops_one, ads_ops_two, ads_ops_three;
    AdsInterface adsInterface;

    public AdsDialog(Context context) {
        super(context);
        adsInterface = (AdsInterface)context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ads_dialog);
        ads_ops_one = (TextView) findViewById(R.id.ads_ops_one);
        ads_ops_two = (TextView) findViewById(R.id.ads_ops_two);
        ads_ops_three = (TextView) findViewById(R.id.ads_ops_three);


        ads_ops_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getContext(), R.string.add_opt1, Toast.LENGTH_SHORT).show();
                adsInterface.OnBuyClickEvent();
                dismiss();
            }
        });

        ads_ops_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(), R.string.add_opt2, Toast.LENGTH_SHORT).show();
                adsInterface.OnRestorClickEvent();
                dismiss();
            }
        });

        ads_ops_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


    }
}
