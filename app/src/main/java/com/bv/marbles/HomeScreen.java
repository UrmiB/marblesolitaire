package com.bv.marbles;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;


import com.bv.marbles.util.IabHelper;
import com.bv.marbles.util.IabResult;
import com.bv.marbles.util.Inventory;
import com.bv.marbles.util.Purchase;
import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
//import com.google.android.gms.auth.api.Auth;
//import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.plus.Plus;
import com.google.example.games.basegameutils.BaseGameActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HomeScreen extends BaseGameActivity implements GoogleApiClient.OnConnectionFailedListener, AdsInterface {
    Boolean sound_on_off = false, final_sound = true;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Animation zoomin, zoomout, jump;
    MediaPlayer selection;
    GoogleApiClient mgoogleApiClient;
    private final int REQUEST_LEADERBOARD = 007;
    AdRequest adRequest;
    InterstitialAd interstitial;
    // static int RC_SIGN_IN = 9001;
    boolean mResolvingConnectionFailure = false;
    boolean mAutoStartSignInFlow = true;
    boolean mSignInClicked = false, mIntentInProgress;
    private ConnectionResult mConnectionResult;
    boolean mAutoStartSignInflow = true;
    IabHelper mHelper;
    final String ITEM_SKU = "android.test.purchased";
    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0R53TCKfR4Lk+oNF1ABSr/FdHh4Uo3u1fC7O83qVSwoZIw1O9EzDaV2J6CnniuermGMCTtYk2coXRgjgPxD3Y0i30JnuigpYIqKqlB/W4ubGaUiDVEbizEzOwB3p0v31412LydZQHZN3bbi46CxTLvbvVf/MABvGOvPsxOYlfUdMNJXFhzA9aZXsKZ8Jij5rFeqR5EifIox0RzNE/O8EdxGh+bJCtcXw90Cdwm+yLDxxq0Tgh3/bpTRUKQDvOXd9VneaK6r+iteXpUH1IjaI4okYdGJSGcDWDOFsVitiW01FN0T8v4jJa0trDFiSNAVfWH2t4W/2ut6LKkdP014OZwIDAQAB";
    final String TAG = "com.ebookfrenzy.inappbilling";
    boolean mIsPremiumMyReplay = false;
    boolean mIsPremiumPaidModules = false;
    static final String SKU_PREMIUM_ADD = "ads";
    static final int RC_REQUEST = 10001;

    @BindView(R.id.play)
    TextView play;
    @BindView(R.id.leader_board)
    TextView leader_board;
    @BindView(R.id.sound_on)
    TextView btn_sound;
    @BindView(R.id.more_games)
    TextView more_games;
    @BindView(R.id.share)
    TextView share;
    @BindView(R.id.about)
    TextView about;
    @BindView(R.id.no_ads)
    TextView no_ads;
    @BindView(R.id.best_score_text)
    TextView best_score_text;
    @BindView(R.id.banner_AdView)
    AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home_screen);

        ButterKnife.bind(this);

       // best_score_text.setBackgroundResource(R.drawable.shadow);
       // best_score_text.setShadowLayer(30, 0, 0, Color.RED);

        selection = MediaPlayer.create(HomeScreen.this, R.raw.selection);
        zoomin = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
        zoomout = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
        jump = AnimationUtils.loadAnimation(this, R.anim.jump);
        play.setAnimation(jump);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();

        try {
            if (pref.getBoolean("ads", true)) {
                no_ads.setBackgroundDrawable(getResources().getDrawable(R.drawable.ads));
            } else {
                no_ads.setBackgroundDrawable(getResources().getDrawable(R.drawable.no_ads));
            }

        } catch (Exception e) {
            no_ads.setBackgroundDrawable(getResources().getDrawable(R.drawable.ads));
        }

        if (isNetworkAvailable(HomeScreen.this)) {
            try {
                Games.Leaderboards.submitScore(getApiClient(), getString(R.string.number_guesses_leaderboard), pref.getInt("final_best_score", 0));
            }catch (Exception e){
                Log.e("marbles", "GoogleApiClient not connected.");
            }
        }

        ads();
        sound();
        bestScore();

        /*mgoogleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this).addApi(Games.API)
                .addScope(Games.SCOPE_GAMES)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
        if (!mgoogleApiClient.isConnected()) {
            mgoogleApiClient.connect();
        }*/

        //connect_game();
        if (isNetworkAvailable(HomeScreen.this)) {
            beginUserInitiatedSignIn();
        }

        //------------------------ app billing---------------------------
        try {
            mHelper = new IabHelper(HomeScreen.this, base64EncodedPublicKey);
            mHelper.startSetup(new
                                       IabHelper.OnIabSetupFinishedListener() {
                                           public void onIabSetupFinished(IabResult result) {
                                               if (!result.isSuccess()) {
                                                   Log.d(TAG, "In-app Billing setup failed: " +
                                                           result);
                                               } else {
                                                   Log.d(TAG, "In-app Billing is set up OK");
                                               }
                                           }
                                       });
        } catch (Exception e) {
            Toast.makeText(HomeScreen.this, "Error in IabHelper", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * This click event use for go to home screen to play game screen
     */
    @OnClick(R.id.play)
    public void play() {
        click_effect(play);
        selection_play();
        Intent marbleIntent = new Intent(HomeScreen.this, Marbles.class);
        HomeScreen.this.startActivity(marbleIntent);
        overridePendingTransition(0, 0);
    }

    /**
     * This click event use for sound on and off
     */
    @OnClick(R.id.sound_on)
    public void sound_option() {
        if (sound_on_off) {
            click_effect(btn_sound);
            selection.start();
            btn_sound.setBackgroundDrawable(getResources().getDrawable(R.drawable.sound_on));
            sound_on_off = false;
            final_sound = true;
            editor.putBoolean("sound", true);
            editor.commit();
            btn_sound.requestFocusFromTouch();
        } else {
            click_effect(btn_sound);
            selection.start();
            btn_sound.setBackgroundDrawable(getResources().getDrawable(R.drawable.sound_off));
            sound_on_off = true;
            final_sound = false;
            editor.putBoolean("sound", false);
            editor.commit();
            btn_sound.requestFocusFromTouch();
        }
    }

    /**
     * This click event use for go to leader board and show your score.
     */
    @OnClick(R.id.leader_board)
    public void leaderBoard() {
        selection_play();
        click_effect(leader_board);
        if (isNetworkAvailable(HomeScreen.this)) {
            //  Games.Leaderboards.submitScore(getApiClient(), getString(R.string.number_guesses_leaderboard), 21);
            try {
                //Games.Leaderboards.submitScore(getApiClient(), getString(R.string.number_guesses_leaderboard), pref.getInt("final_best_score", 0));
                startActivityForResult(Games.Leaderboards.getLeaderboardIntent(getApiClient(),
                        getString(R.string.number_guesses_leaderboard)), REQUEST_LEADERBOARD);
            } catch (Exception e) {
                Toast.makeText(HomeScreen.this, "Not connected to play games.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(HomeScreen.this, "Check your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This click event use for remove ads- buy and restore option.
     */
    @OnClick(R.id.no_ads)
    public void noAds() {
        selection_play();
        click_effect(no_ads);

        AdsDialog dialog = new AdsDialog(HomeScreen.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

               /* Intent i = new Intent(HomeScreen.this,NewRanDomMarbles.class);
                startActivity(i);*/

    }

    /**
     * This click event use for shareing.
     */
    @OnClick(R.id.share)
    public void share() {
        click_effect(share);
        selection_play();
        shareDrawable(HomeScreen.this, R.drawable.splash_screen, "marbles");
    }

    /**
     * This click event use for go to about page.
     */
    @OnClick(R.id.about)
    public void about() {
        selection_play();
        click_effect(about);
        String about_url = "http://www.brainvire.com";
        if (isNetworkAvailable(HomeScreen.this)) {
            CustomTabsIntent intent = new CustomTabsIntent.Builder()
                    .setToolbarColor(ContextCompat.getColor(HomeScreen.this, R.color.colorPrimary))
                    .setShowTitle(true)
                    //.setStartAnimations(HomeScreen.this, R.anim.slide_in, R.anim.slide_out)
                    .build();
            intent.launchUrl(HomeScreen.this, Uri.parse(getString(R.string.about_url)));
            overridePendingTransition(0, 0);
        } else {
            Toast.makeText(HomeScreen.this, "Check your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This click event use for go to mare game.
     */
    @OnClick(R.id.more_games)
    public void more_games() {
        selection_play();
        click_effect(more_games);
        if (isNetworkAvailable(HomeScreen.this)) {
            String appPackageName = getPackageName();
            appPackageName = "Brainvire Infotech Pvt. Ltd.";
            try {
                startActivity(new Intent(
                        Intent.ACTION_VIEW,
                                    /*Uri.parse("http://play.google.com/store/apps/details?id="
                                            + appPackageName)*/
                        Uri.parse("market://search?q=pub:" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/search?q=pub:"
                                + appPackageName)));
            }
        } else {
            Toast.makeText(HomeScreen.this, "Check your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }


    //-------------------buy game--------------------------------------------------------
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase) {

            if (mHelper == null)
                return;

            if (result.isFailure()) {
                // Handle error
                complain("Your Purchase Failed. " + result);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                Log.e("marbles", "Error purchasing. Authenticity verification failed.");
                // setWaitScreen(false);
                // setWaitScreen(false);
                return;
            }
            if (purchase.getSku().equals(ITEM_SKU)) {
                alert("Thank you for upgrading to premium!");
                mIsPremiumPaidModules = true;
                editor.putBoolean("ads", false);
                editor.commit();
                adsUI();
                consumeItem();
            }

        }
    };

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (mHelper == null)
                return;

            if (result.isFailure()) {
                // Handle failure
                complain("Restore Failed. Try Again..!!" + result);
            } else {
                Purchase premiumPurchaseModules = inventory
                        .getPurchase(ITEM_SKU);
                mIsPremiumPaidModules = (verifyDeveloperPayload(premiumPurchaseModules));
                Log.d(TAG, "User is " + (mIsPremiumPaidModules ? "PREMIUM" : "NOT PREMIUM"));
                if (mIsPremiumPaidModules) {
                    editor.putBoolean("ads", false);
                    editor.commit();
                    adsUI();
                    mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU), mConsumeFinishedListener);
                } else {
                    complain("Your Restore Failed.");
                }
            }

            //---------------------------------------------

            //----------------------------------------
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {
                       /* Log.d(TAG, "In-app Billing btn true");
                        editor.putBoolean("ads", false);
                        editor.commit();*/
                        //clickButton.setEnabled(true);
                    } else {
                        // handle error
                        Log.d(TAG, "In-app handle error");
                    }
                }
            };
    //-------------------end buy game--------------------------------------------------------

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        overridePendingTransition(0, 0);
        // overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        startActivity(intent);

    }

    @Override
    protected void onStart() {
        super.onStart();

        //  ads();
       /* if (mgoogleApiClient != null)
            mgoogleApiClient.connect();*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        adView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adView.resume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // mgoogleApiClient.disconnect();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10001) {
            if (!mHelper.handleActivityResult(requestCode,
                    resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adView.destroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    /**
     * This method use for playing selettion sound effect.
     */
    public void selection_play() {

        if (pref.getBoolean("sound", false)) {
            if (selection.isPlaying()) {
                selection.seekTo(0);
                selection.start();
            } else {
                selection.start();
            }
        }
    }

    /**
     * This method use for check internet is available or not.
     */
    public boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    /**
     * This method use for shareing img and text.
     */
    public void shareDrawable(Context context, int resourceId, String fileName) {
        try {
            //convert drawable resource to bitmap
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId);

            //save bitmap to app cache folder
            //context.getCacheDir()
            File outputFile = new File(getExternalCacheDir(), fileName + ".png");
            FileOutputStream outPutStream = new FileOutputStream(outputFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outPutStream);
            outPutStream.flush();
            outPutStream.close();
            outputFile.setReadable(true, false);

            //share file
            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(outputFile));
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Marbles Solitaire");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "Are you marbles solitaire genius?\n" +
                    "\n" +
                    "Let's play marble solitaire.\n" +
                    "http://www.brainvire.com");
            shareIntent.setType("image/png");
            context.startActivity(shareIntent);
        } catch (Exception e) {
            Toast.makeText(context, "error", Toast.LENGTH_LONG);
        }
    }

    public void displayInterstitial() {
        // If Ads are loaded, show Interstitial else show nothing.
        if (interstitial.isLoaded()) {
            // interstitial.show();
        }
    }

    /**
     * This method use for button click animation.
     */
    public void click_effect(View v) {
        AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
        v.startAnimation(buttonClick);
    }

    /**
     * This method use for showing ads.
     */
    public void ads() {
        // adView.setAdUnitId("ca-app-pub-5463303806595357/4479007893");
        // adView.setAdSize(AdSize.BANNER);
        // MobileAds.initialize(getApplicationContext(), "ca-app-pub-5463303806595357/4479007893");


        if (pref.getBoolean("ads", true)) {

            // MobileAds.initialize(this, "ca-app-pub-5463303806595357/4479007893");

            adRequest = new AdRequest.Builder()
                    // Add a test device to show Test Ads
                    // .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    //.addTestDevice(deviceId)
                    .build();

            // Load ads into Banner Ads
            adView.loadAd(adRequest);
        } else {
            no_ads.setBackgroundDrawable(getResources().getDrawable(R.drawable.no_ads));
        }

        /*if (!pref.getBoolean("asd", false)) {


            // Prepare the Interstitial Ad
            interstitial = new InterstitialAd(HomeScreen.this);
            // Insert the Ad Unit ID
            interstitial.setAdUnitId("ca-app-pub-5463303806595357/4479007893");


            String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            String deviceId = Utils.md5(android_id).toUpperCase();
            Log.i("device id=", deviceId);
//            adView.setAdSize(AdSize.BANNER);


            // Load ads into Interstitial Ads
            interstitial.loadAd(adRequest);

            // Prepare an Interstitial Ad Listener
            interstitial.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    // Call displayInterstitial() function
                    //displayInterstitial();
                }
            });
        }*/

     /*   Chartboost.startWithAppId(this, "59917a8ff6cd451a0ce64b0c", "d7bdd580db5f8863ec54b86d6efac9fdb8658fad");
        Chartboost.onCreate(this);
        Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
        Chartboost.showInterstitial(CBLocation.LOCATION_DEFAULT);*/
    }

    /**
     * This method use for playing selettion sound on or off.
     */
    public void sound() {
        try {
            if (pref.getBoolean("sound", false)) {
                btn_sound.setBackgroundDrawable(getResources().getDrawable(R.drawable.sound_on));
                editor.putBoolean("sound", true);
                editor.commit();

            } else {
                btn_sound.setBackgroundDrawable(getResources().getDrawable(R.drawable.sound_off));
                editor.putBoolean("sound", false);
                editor.commit();
            }
        } catch (Exception e) {

        }
    }

    /**
     * This method use for showing best score.
     */
    public void bestScore() {
        if (pref.getInt("final_best_score", 0) < pref.getInt("best_score", 0)) {
            editor.putInt("final_best_score", pref.getInt("best_score", 0));
            editor.commit();
        }

        try {
            if (pref.getInt("final_best_score", 0) != 0) {
                best_score_text.setText("Best Socre " + String.valueOf(pref.getInt("final_best_score", 0)));
            } else {
                best_score_text.setText(R.string.text_max_moves);
            }
        } catch (Exception e) {
            //best_score_text.setText(R.string.text_max_moves);
        }
    }

    @Override
    public void OnBuyClickEvent() {
        //Toast.makeText(HomeScreen.this, "buy", Toast.LENGTH_SHORT).show();
        mHelper.launchPurchaseFlow(HomeScreen.this, ITEM_SKU, RC_REQUEST, mPurchaseFinishedListener, "mypurchasetoken");
    }

    @Override
    public void OnRestorClickEvent() {
        //Toast.makeText(HomeScreen.this, "Restore ", Toast.LENGTH_SHORT).show();
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

		/*
         * TODO: verify that the developer payload of the purchase is correct.
		 * It will be the same one that you sent when initiating the purchase.
		 *
		 * WARNING: Locally generating a random string when starting a purchase
		 * and verifying it here might seem like a good approach, but this will
		 * fail in the case where the user purchases an item on one device and
		 * then uses your app on a different device, because on the other device
		 * you will not have access to the random string you originally
		 * generated.
		 *
		 * So a good developer payload has these characteristics:
		 *
		 * 1. If two different users purchase an item, the payload is different
		 * between them, so that one user's purchase can't be replayed to
		 * another user.
		 *
		 * 2. The payload must be such that you can verify it even when the app
		 * wasn't the one who initiated the purchase flow (so that items
		 * purchased by the user on one device work on other devices owned by
		 * the user).
		 *
		 * Using your own server to store and verify developer payloads across
		 * app installations is recommended.
		 */

        return true;
    }

    public void adsUI() {
        if (pref.getBoolean("ads", true)) {
            no_ads.setBackgroundDrawable(getResources().getDrawable(R.drawable.ads));
        } else {
            no_ads.setBackgroundDrawable(getResources().getDrawable(R.drawable.no_ads));
        }
    }

    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }

    void complain(String message) {
        Log.e("marbles", "Error. Try Again..!!" + message);
        alert("Error: " + message);
    }
}

