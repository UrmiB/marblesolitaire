package com.bv.marbles;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lukedeighton.wheelview.WheelView;
import com.lukedeighton.wheelview.adapter.WheelAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    TextView m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, title_label, score_label, restart, new_screen;
    Boolean bm1 = true, bm2 = true, bm3 = false, bm4 = true, bm5 = true, bm6 = true, bm7 = true, bm8 = true, bm9 = true, bm10 = true, bm11 = true, bm12 = true;
    String mar = "";
    int score = 0;
    WheelView wheelview;
    private ArrayList<Drawable> list;
FrameLayout main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        list = new ArrayList<>();
        for(int i=0;i<=32;i++) {
            list.add(getResources().getDrawable(R.drawable.marbles7));
        }
         wheelview = (WheelView)findViewById(R.id.wheelview);
        wheelview.setClickable(false);
        wheelview.setEnabled(false);
        wheelview.setFocusableInTouchMode(false);
        wheelview.setFocusable(false);
        wheelview.setHorizontalScrollBarEnabled(false);
        wheelview.setVerticalScrollBarEnabled(false);

        wheelview.setAdapter(new WheelAdapter() {

            @Override
            public Drawable getDrawable(int position) {

                return list.get(position);
            }

            @Override
            public int getCount() {
                return list.size();
            }

            @Override
            public Object getItem(int position) {
                return list.get(position);
            }
        });

        // Set fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Set No Title
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        title_label = (TextView) findViewById(R.id.title_label);
        score_label = (TextView) findViewById(R.id.score_label);
        restart = (TextView) findViewById(R.id.restart);
        new_screen = (TextView) findViewById(R.id.new_screen);
        m1 = (TextView) findViewById(R.id.m1);
        m2 = (TextView) findViewById(R.id.m2);
        m3 = (TextView) findViewById(R.id.m3);
        m4 = (TextView) findViewById(R.id.m4);
        m5 = (TextView) findViewById(R.id.m5);
        m6 = (TextView) findViewById(R.id.m6);
        m7 = (TextView) findViewById(R.id.m7);
        m8 = (TextView) findViewById(R.id.m8);
        m9 = (TextView) findViewById(R.id.m9);
        m10 = (TextView) findViewById(R.id.m10);
        m11 = (TextView) findViewById(R.id.m11);
        m12 = (TextView) findViewById(R.id.m12);
        main = (FrameLayout)findViewById(R.id.main);
        newgame();
        final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
        final Animation jump = AnimationUtils.loadAnimation(this, R.anim.jump);

        Drawable dm1 = m1.getBackground();
        m1.setBackground(dm1);


        score_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                score_label.startAnimation(jump);
                new MarblesAnimation().attachActivity(MainActivity.this).setTargetView(score_label).setDestView(restart).startAnimation();
            }
        });

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setTitle("Restart game");
                builder1.setMessage("Are you sure ?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                newgame();
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder1.show();
            }
        });

        new_screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Marbles.class);
                startActivity(i);
            }
        });

        m1.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      //blink.setDuration(2000);
                                      //m1.startAnimation(jump);
                                   //   animatorDisappearSet.playTogether(disappearAnimatorX, disappearAnimatorY);
                                      if (bm1) {
                                          if (!bm3 && bm2 || !bm9 && bm5) {
                                              if (!bm3 && bm2) {
                                                  clickabletrue(m3);
                                              }
                                              if (!bm9 && bm5) {
                                                  clickabletrue(m9);
                                              }
                                              title_label.setText(R.string.marble);
                                          } else {
                                              title_label.setText(R.string.wrong_marble);
                                          }
                                      } else {
                                          if (mar.equalsIgnoreCase("mar3")) {
                                              marblesChange(m3, m2, m1);
                                              bm3 = false;
                                              bm2 = false;
                                              bm1 = true;
                                          } else if (mar.equalsIgnoreCase("mar9")) {
                                              marblesChange(m9, m5, m1);
                                              bm9 = false;
                                              bm5 = false;
                                              bm1 = true;
                                          }
                                          gameover();
                                      }
                                      mar = "mar1";

                                  }
                              }

        );
        m2.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      if (bm2) {
                                          if (!bm4 && bm3 || !bm10 && bm6) {
                                              if (!bm4 && bm3) {
                                                  clickabletrue(m4);

                                              }
                                              if (!bm10 && bm6) {
                                                  clickabletrue(m10);
                                              }
                                              title_label.setText(R.string.marble);
                                          } else {
                                              title_label.setText(R.string.wrong_marble);
                                          }
                                      } else {
                                          if (mar.equalsIgnoreCase("mar4")) {
                                              marblesChange(m4, m3, m2);
                                              bm4 = false;
                                              bm3 = false;
                                              bm2 = true;

                                          } else if (mar.equalsIgnoreCase("mar10")) {
                                              marblesChange(m10, m6, m2);
                                              bm10 = false;
                                              bm6 = false;
                                              bm2 = true;
                                          }
                                          gameover();
                                      }
                                      mar = "mar2";
                                  }
                              }

        );

        m3.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      if (bm3) {
                                          if (!bm1 && bm2 || !bm11 && bm7) {
                                              if (!bm1 && bm2) {
                                                  clickabletrue(m1);
                                              }
                                              if (!bm11 && bm7) {
                                                  clickabletrue(m11);
                                              }
                                              title_label.setText(R.string.marble);
                                          } else {
                                              title_label.setText(R.string.wrong_marble);
                                          }
                                      } else {

                                          if (mar.equalsIgnoreCase("mar1")) {
                                              marblesChange(m1, m2, m3);
                                              bm1 = false;
                                              bm2 = false;
                                              bm3 = true;

                                          } else if (mar.equalsIgnoreCase("mar11")) {
                                              marblesChange(m11, m7, m3);
                                              bm11 = false;
                                              bm7 = false;
                                              bm3 = true;
                                          }
                                          gameover();
                                      }
                                      mar = "mar3";
                                  }
                              }

        );

        m4.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      if (bm4) {
                                          if (!bm2 && bm3 || !bm12 && bm8) {
                                              if (!bm2 && bm3) {
                                                  clickabletrue(m2);
                                              }
                                              if (!bm12 && bm8) {
                                                  clickabletrue(m12);
                                              }
                                              title_label.setText(R.string.marble);
                                          } else {
                                              title_label.setText(R.string.wrong_marble);
                                          }
                                      } else {
                                          if (mar.equalsIgnoreCase("mar2")) {
                                              marblesChange(m2, m3, m4);
                                              bm2 = false;
                                              bm3 = false;
                                              bm4 = true;
                                          } else if (mar.equalsIgnoreCase("mar12")) {
                                              marblesChange(m12, m8, m4);
                                              bm12 = false;
                                              bm8 = false;
                                              bm4 = true;
                                          }
                                          gameover();
                                      }
                                      mar = "mar4";
                                  }
                              }

        );

        m5.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      if (bm5) {
                                          if (!bm7 && bm6) {
                                              clickabletrue(m7);
                                              title_label.setText(R.string.marble);

                                          } else {
                                              title_label.setText(R.string.wrong_marble);
                                          }
                                      } else {
                                          if (mar.equalsIgnoreCase("mar7")) {
                                              marblesChange(m7, m6, m5);
                                              bm7 = false;
                                              bm6 = false;
                                              bm5 = true;
                                          }
                                          gameover();
                                      }
                                      mar = "mar5";
                                  }
                              }

        );
        m6.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      if (bm6) {
                                          if (!bm8 && bm7) {
                                              clickabletrue(m8);
                                              title_label.setText(R.string.marble);

                                          } else {
                                              title_label.setText(R.string.wrong_marble);
                                          }
                                      } else {
                                          if (mar.equalsIgnoreCase("mar8")) {
                                              marblesChange(m8, m7, m6);
                                              bm8 = false;
                                              bm7 = false;
                                              bm6 = true;
                                          }
                                          gameover();
                                      }
                                      mar = "mar6";
                                  }
                              }

        );
        m7.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      if (bm7) {
                                          if (!bm5 && bm6) {
                                              clickabletrue(m5);
                                              title_label.setText(R.string.marble);

                                          } else {
                                              title_label.setText(R.string.wrong_marble);
                                          }
                                      } else {
                                          if (mar.equalsIgnoreCase("mar5")) {
                                              marblesChange(m5, m6, m7);
                                              bm5 = false;
                                              bm6 = false;
                                              bm7 = true;
                                          }
                                          gameover();
                                      }
                                      mar = "mar7";
                                  }
                              }

        );
        m8.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      if (bm8) {
                                          if (!bm6 && bm7) {
                                              clickabletrue(m6);
                                              title_label.setText(R.string.marble);

                                          } else {
                                              title_label.setText(R.string.wrong_marble);
                                          }
                                      } else {
                                          if (mar.equalsIgnoreCase("mar6")) {
                                              marblesChange(m6, m7, m8);
                                              bm6 = false;
                                              bm7 = false;
                                              bm8 = true;

                                          }
                                          gameover();
                                      }
                                      mar = "mar8";
                                  }
                              }

        );
        m9.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      if (bm9) {
                                          if (!bm1 && bm5 || !bm11 && bm10) {
                                              if (!bm1 && bm5) {
                                                  clickabletrue(m1);
                                              }
                                              if (!bm11 && bm10) {
                                                  clickabletrue(m11);
                                              }
                                              title_label.setText(R.string.marble);
                                          } else {
                                              title_label.setText(R.string.wrong_marble);
                                          }
                                      } else {
                                          if (mar.equalsIgnoreCase("mar1")) {
                                              marblesChange(m1, m5, m9);
                                              bm1 = false;
                                              bm5 = false;
                                              bm9 = true;
                                          } else if (mar.equalsIgnoreCase("mar11")) {
                                              marblesChange(m11, m10, m9);
                                              bm11 = false;
                                              bm10 = false;
                                              bm9 = true;
                                          }
                                          gameover();
                                      }
                                      mar = "mar9";
                                  }
                              }

        );
        m10.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       if (bm10) {
                                           if (!bm2 && bm6 || !bm12 && bm11) {
                                               if (!bm2 && bm6) {
                                                   clickabletrue(m2);
                                               }
                                               if (!bm12 && bm11) {
                                                   clickabletrue(m12);
                                               }
                                               title_label.setText(R.string.marble);
                                           } else {
                                               title_label.setText(R.string.wrong_marble);
                                           }
                                       } else {
                                           if (mar.equalsIgnoreCase("mar2")) {
                                               marblesChange(m2, m6, m10);
                                               bm2 = false;
                                               bm6 = false;
                                               bm10 = true;
                                           } else if (mar.equalsIgnoreCase("mar12")) {
                                               marblesChange(m12, m11, m10);
                                               bm12 = false;
                                               bm11 = false;
                                               bm10 = true;
                                           }
                                           gameover();
                                       }
                                       mar = "mar10";
                                   }
                               }

        );
        m11.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       if (bm11) {
                                           if (!bm9 && bm10 || !bm3 && bm7) {
                                               if (!bm9 && bm10) {
                                                   clickabletrue(m9);
                                               }
                                               if (!bm3 && bm7) {
                                                   clickabletrue(m3);
                                               }
                                               title_label.setText(R.string.marble);
                                           } else {
                                               title_label.setText(R.string.wrong_marble);
                                           }
                                       } else {
                                           if (mar.equalsIgnoreCase("mar9")) {
                                               marblesChange(m9, m10, m11);
                                               bm9 = false;
                                               bm10 = false;
                                               bm11 = true;
                                           } else if (mar.equalsIgnoreCase("mar3")) {
                                               marblesChange(m3, m7, m11);
                                               bm3 = false;
                                               bm7 = false;
                                               bm11 = true;
                                           }
                                           gameover();
                                       }
                                       mar = "mar11";
                                   }
                               }

        );
        m12.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       if (bm12) {
                                           if (!bm10 && bm11 || !bm4 && bm8) {
                                               if (!bm10 && bm11) {
                                                   clickabletrue(m10);
                                               }
                                               if (!bm4 && bm8) {
                                                   clickabletrue(m4);
                                               }
                                               title_label.setText(R.string.marble);
                                           } else {
                                               title_label.setText(R.string.wrong_marble);
                                           }
                                       } else {
                                           if (mar.equalsIgnoreCase("mar10")) {
                                               marblesChange(m10, m11, m12);
                                               bm10 = false;
                                               bm11 = false;
                                               bm12 = true;
                                           } else if (mar.equalsIgnoreCase("mar4")) {
                                               marblesChange(m4, m8, m12);
                                               bm4 = false;
                                               bm8 = false;
                                               bm12 = true;
                                           }
                                           gameover();
                                       }
                                       mar = "mar12";
                                   }
                               }
        );


    }

    public void marblesChange(final View black, final View black2, final View marble) {
        final Drawable to_marble = black.getBackground();
        new MarblesAnimation().attachActivity(MainActivity.this).setTargetView(black).setDestView(marble).startAnimation();
        //----


        //------
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                black.setBackgroundDrawable(getResources().getDrawable(R.drawable.black));
                black2.setBackgroundDrawable(getResources().getDrawable(R.drawable.black));
                marble.setBackground(to_marble);
                score = score + 1;
                title_label.setText(R.string.nice_play);
                score_label.setText("Score : " + score);
                clickablefalse(black);
                clickablefalse(black2);
            }
        }, 700);


    }

    public void clickablefalse(View v) {
        v.setClickable(false);
        v.setFocusable(false);
        v.setFocusableInTouchMode(false);

    }

    public void clickabletrue(View v) {
        v.setClickable(true);
        v.setFocusable(true);
        v.setFocusableInTouchMode(false);

    }

    public void gameover() {
        if (bm1 && (!bm3 && bm2 || !bm9 && bm5)) {

        } else if (bm2 && (!bm4 && bm3 || !bm10 && bm6)) {

        } else if (bm3 && (!bm1 && bm2 || !bm11 && bm7)) {

        } else if (bm4 && (!bm2 && bm3 || !bm12 && bm8)) {

        } else if (bm5 && (!bm7 && bm6)) {

        } else if (bm6 && (!bm8 && bm7)) {

        } else if (bm7 && (!bm5 && bm6)) {

        } else if (bm8 && (!bm6 && bm7)) {

        } else if (bm9 && (!bm1 && bm5 || !bm11 && bm10)) {

        } else if (bm10 && (!bm2 && bm6 || !bm12 && bm11)) {

        } else if (bm11 && (!bm9 && bm10 || !bm3 && bm7)) {

        } else if (bm12 && (!bm10 && bm11 || !bm4 && bm8)) {

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    int extra_marbles = 11 - score;
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                    builder1.setTitle("Game Over.");
                    builder1.setMessage("Score :" + score+"\nMarbles :" + extra_marbles);
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //newgame();
                                    dialog.cancel();
                                    Intent i = new Intent(MainActivity.this, Marbles.class);
                                    startActivity(i);
                                }
                            });

           /* builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });*/
                    builder1.show();
                }
            }, 700);


        }
    }


    public void newgame() {
        bm1 = bm2 = bm4 = bm5 = bm6 = bm7 = bm8 = bm9 = bm10 = bm11 = bm12 = true;
        bm3 = false;
        mar = "";
        score = 0;
        m1.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles2));
        m2.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles3));
        m3.setBackgroundDrawable(getResources().getDrawable(R.drawable.black));
        m4.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles3));
        m5.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles7));
        m6.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles2));
        m7.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles8));
        m8.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles5));
        m9.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles4));
        m10.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles2));
        m11.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles5));
        m12.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles7));

        title_label.setText("");
        score_label.setText(R.string.score);
        clickabletrue(m1);
        clickabletrue(m2);
        clickabletrue(m3);
        clickabletrue(m4);
        clickabletrue(m5);
        clickabletrue(m6);
        clickabletrue(m7);
        clickabletrue(m8);
        clickabletrue(m9);
        clickabletrue(m10);
        clickabletrue(m11);
        clickabletrue(m12);
    }

    public void done_marbles() {
        int numViews = 31;
        for (int i = 0; i < numViews; i++) {
            // Create some quick TextViews that can be placed.

            // Set a text and center it in each view.
           /* if(i==0)
            v.setBackgroundDrawable(getResources().getDrawable(R.drawable.mar5));
            else*/
            TextView v = new TextView(this);
            //v.setBackgroundDrawable(getResources().getDrawable(R.drawable.black));
            //v.setBackgroundColor(ContextCompat.getColor(this, R.color.invisible_color));
            v.setBackgroundResource(0);
            // v.setImageResource(null);
            //v.setBackgroundDrawable(getResources().getDrawable(R.drawable.invisible));

            for (int j = 0; j <= score; j++) {
                try {
                    v.setBackgroundDrawable(getResources().getDrawable(R.drawable.un_selected_marbles));
                } catch (Exception e) {
                    v.setBackgroundDrawable(getResources().getDrawable(R.drawable.un_selected_marbles));
                }
            }
            v.setGravity(Gravity.CENTER);


            // Force the views to a nice size (150x100 px) that fits my display.
            // This should of course be done in a display size independent way.
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(70, 70);
            // Place all views in the center of the layout. We'll transform them
            // away from there in the code below.
            lp.gravity = Gravity.CENTER;
            // Set layout params on view.
            v.setLayoutParams(lp);

            // Calculate the angle of the current view. Adjust by 90 degrees to
            // get View 0 at the top. We need the angle in degrees and radians.
            float angleDeg = i * 360.0f / numViews - 90.0f;
            float angleRad = (float) (angleDeg * Math.PI / 180.0f);
            // Calculate the position of the view, offset from center (300 px from
            // center). Again, this should be done in a display size independent way.
            v.setTranslationX(450 * (float) Math.cos(angleRad));
            v.setTranslationY(450 * (float) Math.sin(angleRad));
            // Set the rotation of the view.
            v.setRotation(angleDeg + 90.0f);
            main.addView(v);
        }
    }
}
