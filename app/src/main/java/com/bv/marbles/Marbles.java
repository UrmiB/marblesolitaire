package com.bv.marbles;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.games.Games;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Marbles extends AppCompatActivity {

    Boolean bm1 = true, bm2 = true, bm3 = true, bm4 = true, bm5 = true, bm6 = true, bm7 = true, bm8 = true, bm9 = true, bm10 = true, bm11 = true, bm12 = true;
    Boolean bm13 = true, bm14 = true, bm15 = true, bm16 = true, bm17 = false, bm18 = true, bm19 = true, bm20 = true, bm21 = true, bm22 = true, bm23 = true, bm24 = true;
    Boolean bm25 = true, bm26 = true, bm27 = true, bm28 = true, bm29 = true, bm30 = true, bm31 = true, bm32 = true, bm33 = true;
    String mar = "";
    int score = 0;
    List<Drawable> imag_id = new ArrayList<>();
    MediaPlayer discard_marble, error_sound, selection;
    Boolean sound_on_off = false, final_sound = true;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    int width, height;
    Vibrator vib;
    Animation animShake, slide, slideup;
    Boolean game_over_key = false;
    InterstitialAd mInterstitialAd;
    AdRequest adRequest;
    int r_n, ads_count = 1;
    Animation score_jump;

    @BindView(R.id.sound)
    TextView btnsound;
    @BindView(R.id.home)
    TextView home;
    @BindView(R.id.hint)
    TextView hint;
    @BindView(R.id.help)
    TextView help;
    @BindView(R.id.restart)
    TextView restart;
    @BindView(R.id.title_label)
    TextView title_label;
    @BindView(R.id.score_label)
    TextView score_label;
    @BindView(R.id.marbles_board)
    TextView marbles_board;
    @BindView(R.id.game_over_text)
    TextView game_over_text;
    @BindView(R.id.blankmarble)
    TextView blankmarble;
    @BindView(R.id.activity_marbles)
    RelativeLayout activity_marbles;
    @BindView(R.id.ll_gameover)
    LinearLayout ll_gameover;
    @BindView(R.id.main)
    FrameLayout main;
    @BindView(R.id.m1)
    TextView m1;
    @BindView(R.id.m2)
    TextView m2;
    @BindView(R.id.m3)
    TextView m3;
    @BindView(R.id.m4)
    TextView m4;
    @BindView(R.id.m5)
    TextView m5;
    @BindView(R.id.m6)
    TextView m6;
    @BindView(R.id.m7)
    TextView m7;
    @BindView(R.id.m8)
    TextView m8;
    @BindView(R.id.m9)
    TextView m9;
    @BindView(R.id.m10)
    TextView m10;
    @BindView(R.id.m11)
    TextView m11;
    @BindView(R.id.m12)
    TextView m12;
    @BindView(R.id.m13)
    TextView m13;
    @BindView(R.id.m14)
    TextView m14;
    @BindView(R.id.m15)
    TextView m15;
    @BindView(R.id.m16)
    TextView m16;
    @BindView(R.id.m17)
    TextView m17;
    @BindView(R.id.m18)
    TextView m18;
    @BindView(R.id.m19)
    TextView m19;
    @BindView(R.id.m20)
    TextView m20;
    @BindView(R.id.m21)
    TextView m21;
    @BindView(R.id.m22)
    TextView m22;
    @BindView(R.id.m23)
    TextView m23;
    @BindView(R.id.m24)
    TextView m24;
    @BindView(R.id.m25)
    TextView m25;
    @BindView(R.id.m26)
    TextView m26;
    @BindView(R.id.m27)
    TextView m27;
    @BindView(R.id.m28)
    TextView m28;
    @BindView(R.id.m29)
    TextView m29;
    @BindView(R.id.m30)
    TextView m30;
    @BindView(R.id.m31)
    TextView m31;
    @BindView(R.id.m32)
    TextView m32;
    @BindView(R.id.m33)
    TextView m33;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marbles);
        ButterKnife.bind(this);

        //device height and width
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int d_height = displayMetrics.heightPixels;
        int d_width = displayMetrics.widthPixels;

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        animShake = AnimationUtils.loadAnimation(Marbles.this, R.anim.shake_screen);
        slide = AnimationUtils.loadAnimation(this, R.anim.game_over_slide);
        slideup = AnimationUtils.loadAnimation(this, R.anim.zoom_out);

        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        width = metrics.widthPixels;
        height = metrics.heightPixels;
        //Toast.makeText(this, width + "," + height, Toast.LENGTH_SHORT).show();
        System.out.println("{" + width + "," + height + "}");

        selection = MediaPlayer.create(this, R.raw.selection);
        error_sound = MediaPlayer.create(this, R.raw.error_sound);
        discard_marble = MediaPlayer.create(this, R.raw.discard_marble);

        newgame();

        //done_marbles();

        //sound

        if (pref.getBoolean("sound", false)) {
            btnsound.setBackgroundDrawable(getResources().getDrawable(R.drawable.sound_on));
            editor.putBoolean("sound", true);
            editor.commit();

        } else {
            btnsound.setBackgroundDrawable(getResources().getDrawable(R.drawable.sound_off));
            editor.putBoolean("sound", false);
            editor.commit();
        }

    }
    /**
     * This click event use for restart game.
     * */
    @OnClick(R.id.restart)
    public void restart() {
        click_effect(restart);
        btn_selection_play();
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Marbles.this);
        builder1.setTitle(getResources().getString(R.string.game_name));
        builder1.setMessage(getResources().getString(R.string.restart_msg));
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = getIntent();

                        startActivity(intent);
                        finish();
                        overridePendingTransition(0, 0);
                        //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        //newgame();
                        imag_id.clear();
                        // done_marbles();
                        dialog.cancel();

                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        if (game_over_key) {
            Intent intent = getIntent();
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
            //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            //newgame();
            imag_id.clear();
            // done_marbles();
        } else {
            builder1.show();
        }
    }

    /**
     * This click event use for sound on off.
     * */
    @OnClick(R.id.sound)
    public void sound() {
        if (sound_on_off) {
            click_effect(btnsound);
            selection.start();
            btnsound.setBackgroundDrawable(getResources().getDrawable(R.drawable.sound_on));
            sound_on_off = false;
            final_sound = true;
            editor.putBoolean("sound", true);
            editor.commit();
            btnsound.setFocusableInTouchMode(true);
            btnsound.setFocusable(true);
            btnsound.requestFocus();

        } else {
            click_effect(btnsound);
            selection.start();
            btnsound.setBackgroundDrawable(getResources().getDrawable(R.drawable.sound_off));
            sound_on_off = true;
            final_sound = false;
            editor.putBoolean("sound", false);
            editor.commit();
            btnsound.setFocusableInTouchMode(true);
            btnsound.setFocusable(true);
            btnsound.requestFocus();
        }
    }

    /**
     * This click event use for go to home page.
     * */
    @OnClick(R.id.home)
    public void home() {
        click_effect(home);
        btn_selection_play();

        AlertDialog.Builder builder2 = new AlertDialog.Builder(Marbles.this);
        builder2.setTitle(getResources().getString(R.string.game_name));
        builder2.setMessage(getResources().getString(R.string.home_msg));
        builder2.setCancelable(true);

        builder2.setPositiveButton(
                "yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent mainIntent = new Intent(Marbles.this, HomeScreen.class);
                        startActivity(mainIntent);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        overridePendingTransition(0, 0);
                        //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        // done_marbles();
                        showAds();
                        dialog.cancel();

                    }
                });

        builder2.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        if (game_over_key) {
            Intent mainIntent = new Intent(Marbles.this, HomeScreen.class);
            startActivity(mainIntent);
            showAds();
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            overridePendingTransition(0, 0);
            //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        } else {
            builder2.show();
        }
    }

    /**
     * This click event use for hint in game.
     * */
    @OnClick(R.id.hint)
    public void hints() {
        click_effect(hint);
        btn_selection_play();
        hint();
    }

    /**
     * This click event use for go to home page to how to play screen.
     * */
    @OnClick(R.id.help)
    public void help() {
        click_effect(help);
        btn_selection_play();
        //discard_marblen_sound_play();
        Intent i = new Intent(Marbles.this, HowToPlay.class);
        startActivity(i);
        overridePendingTransition(0, 0);
        //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    /**
     * m1 to m33 click event use for select marble,jumping marble and moving marbles.
     * */
    @OnClick(R.id.m1)
    public void m1() {
        if (!m1.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m1).setDestView(m1).startAnimation();
        if (bm1) {
            if (!bm3 && bm2 || !bm9 && bm4) {
                if (!bm3 && bm2) {
                    clickabletrue(m3);
                }
                if (!bm9 && bm4) {
                    clickabletrue(m9);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar3") && marble_drw(m3, m2)) {
                marblesChange(m3, m2, m1);
                bm3 = false;
                bm2 = false;
                bm1 = true;
            } else if (mar.equalsIgnoreCase("mar9") && marble_drw(m9, m4)) {
                marblesChange(m9, m4, m1);
                bm9 = false;
                bm4 = false;
                bm1 = true;
            }
            gameover();
        }
        mar = "mar1";
    }

    @OnClick(R.id.m2)
    public void m2() {
        if (!m2.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m2).setDestView(m2).startAnimation();
        if (bm2) {
            if (!bm10 && bm5) {
                clickabletrue(m10);
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar10") && marble_drw(m10, m5)) {
                marblesChange(m10, m5, m2);
                bm10 = false;
                bm5 = false;
                bm2 = true;
            }
            gameover();
        }
        mar = "mar2";
    }

    @OnClick(R.id.m3)
    public void m3() {
        if (!m3.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m3).setDestView(m3).startAnimation();
        if (bm3) {
            if (!bm11 && bm6 || !bm1 && bm2) {
                if (!bm11 && bm6) {
                    clickabletrue(m11);
                }
                if (!bm1 && bm2) {
                    clickabletrue(m1);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar11") && marble_drw(m11, m6)) {
                marblesChange(m11, m6, m3);
                bm11 = false;
                bm6 = false;
                bm3 = true;
            } else if (mar.equalsIgnoreCase("mar1") && marble_drw(m1, m2)) {
                marblesChange(m1, m2, m3);
                bm1 = false;
                bm2 = false;
                bm3 = true;
            }
            gameover();
        }
        mar = "mar3";
    }

    @OnClick(R.id.m4)
    public void m4() {
        if (!m4.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m4).setDestView(m4).startAnimation();
        if (bm4) {
            if (!bm6 && bm5 || !bm16 && bm9) {
                if (!bm6 && bm5) {
                    clickabletrue(m6);
                }
                if (!bm16 && bm9) {
                    clickabletrue(m16);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));
            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            //--------------------------------------------------
                                         /* Log.e("log", "bolarojo: "+getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState().toString());
                                          Log.e("log", "bolaclic: "+m4.getBackground().getConstantState().toString());
                                          if(!m4.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState())) {
                                              Log.e("log", "same");
                                          }
                                          else
                                              Log.e("log", "no");*/
            //------------------------------------------------------
            //black.setBackgroundDrawable(getResources().getDrawable(R.drawable.un_selected_marbles));
            if (mar.equalsIgnoreCase("mar6") && marble_drw(m6, m5)) {
                marblesChange(m6, m5, m4);
                bm6 = false;
                bm5 = false;
                bm4 = true;
            } else if (mar.equalsIgnoreCase("mar16") && marble_drw(m16, m9)) {
                //if (!m9.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState())) {
                Log.e("log", "same");
                marblesChange(m16, m9, m4);
                bm16 = false;
                bm9 = false;
                bm4 = true;
            }
            gameover();
        }
        mar = "mar4";
    }

    @OnClick(R.id.m5)
    public void m5() {
        if (!m5.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m5).setDestView(m5).startAnimation();
        if (bm5) {
            if (!bm17 && bm10) {
                clickabletrue(m17);
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar17") && marble_drw(m17, m10)) {
                marblesChange(m17, m10, m5);
                bm17 = false;
                bm10 = false;
                bm5 = true;
            }
            gameover();
        }
        mar = "mar5";
    }

    @OnClick(R.id.m6)
    public void m6() {
        if (!m6.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m6).setDestView(m6).startAnimation();
        if (bm6) {
            if (!bm4 && bm5 || !bm18 && bm11) {
                if (!bm4 && bm5) {
                    clickabletrue(m4);
                }
                if (!bm18 && bm11) {
                    clickabletrue(m18);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar4") && marble_drw(m4, m5)) {
                marblesChange(m4, m5, m6);
                bm4 = false;
                bm5 = false;
                bm6 = true;
            } else if (mar.equalsIgnoreCase("mar18") && marble_drw(m18, m11)) {
                marblesChange(m18, m11, m6);
                bm18 = false;
                bm11 = false;
                bm6 = true;
            }
            gameover();
        }
        mar = "mar6";
    }

    @OnClick(R.id.m7)
    public void m7() {
        if (!m7.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m7).setDestView(m7).startAnimation();
        if (bm7) {
            if (!bm9 && bm8 || !bm21 && bm14) {
                if (!bm9 && bm8) {
                    clickabletrue(m9);
                }
                if (!bm21 && bm14) {
                    clickabletrue(m21);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar9") && marble_drw(m9, m8)) {
                marblesChange(m9, m8, m7);
                bm9 = false;
                bm8 = false;
                bm7 = true;
            } else if (mar.equalsIgnoreCase("mar21") && marble_drw(m21, m14)) {
                marblesChange(m21, m14, m7);
                bm21 = false;
                bm14 = false;
                bm7 = true;
            }
            gameover();
        }
        mar = "mar7";
    }

    @OnClick(R.id.m8)
    public void m8() {
        if (!m8.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m8).setDestView(m8).startAnimation();
        if (bm8) {
            if (!bm10 && bm9 || !bm22 && bm15) {
                if (!bm10 && bm9) {
                    clickabletrue(m10);
                }
                if (!bm22 && bm15) {
                    clickabletrue(m22);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar10") && marble_drw(m10, m9)) {
                marblesChange(m10, m9, m8);
                bm10 = false;
                bm9 = false;
                bm8 = true;
            } else if (mar.equalsIgnoreCase("mar22") && marble_drw(m22, m15)) {
                marblesChange(m22, m15, m8);
                bm22 = false;
                bm15 = false;
                bm8 = true;
            }
            gameover();
        }
        mar = "mar8";
    }

    @OnClick(R.id.m9)
    public void m9() {
        if (!m9.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m9).setDestView(m9).startAnimation();
        if (bm9) {
            if (!bm1 && bm4 || !bm7 && bm8 || !bm11 && bm10 || !bm23 && bm16) {
                if (!bm1 && bm4) {
                    clickabletrue(m1);
                }
                if (!bm7 && bm8) {
                    clickabletrue(m7);
                }
                if (!bm11 && bm10) {
                    clickabletrue(m11);
                }
                if (!bm23 && bm16) {
                    clickabletrue(m23);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar1") && marble_drw(m1, m4)) {
                marblesChange(m1, m4, m9);
                bm1 = false;
                bm4 = false;
                bm9 = true;
            } else if (mar.equalsIgnoreCase("mar7") && marble_drw(m7, m8)) {
                marblesChange(m7, m8, m9);
                bm7 = false;
                bm8 = false;
                bm9 = true;
            } else if (mar.equalsIgnoreCase("mar11") && marble_drw(m11, m10)) {
                marblesChange(m11, m10, m9);
                bm11 = false;
                bm10 = false;
                bm9 = true;
            } else if (mar.equalsIgnoreCase("mar23") && marble_drw(m23, m16)) {
                marblesChange(m23, m16, m9);
                bm23 = false;
                bm16 = false;
                bm9 = true;
            }
            gameover();
        }
        mar = "mar9";
    }

    @OnClick(R.id.m10)
    public void m10() {
        if (!m10.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m10).setDestView(m10).startAnimation();
        if (bm10) {
            if (!bm2 && bm5 || !bm8 && bm9 || !bm24 && bm17 || !bm12 && bm11) {
                if (!bm2 && bm5) {
                    clickabletrue(m2);
                }
                if (!bm8 && bm9) {
                    clickabletrue(m8);
                }
                if (!bm24 && bm17) {
                    clickabletrue(m24);
                }
                if (!bm12 && bm11) {
                    clickabletrue(m12);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar2") && marble_drw(m2, m5)) {
                marblesChange(m2, m5, m10);
                bm2 = false;
                bm5 = false;
                bm10 = true;
            } else if (mar.equalsIgnoreCase("mar8") && marble_drw(m8, m9)) {
                marblesChange(m8, m9, m10);
                bm8 = false;
                bm9 = false;
                bm10 = true;
            } else if (mar.equalsIgnoreCase("mar24") && marble_drw(m24, m17)) {
                marblesChange(m24, m17, m10);
                bm24 = false;
                bm17 = false;
                bm10 = true;
            } else if (mar.equalsIgnoreCase("mar12") && marble_drw(m12, m11)) {
                marblesChange(m12, m11, m10);
                bm12 = false;
                bm11 = false;
                bm10 = true;
            }
            gameover();
        }
        mar = "mar10";
    }

    @OnClick(R.id.m11)
    public void m11() {
        if (!m11.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m11).setDestView(m11).startAnimation();
        if (bm11) {
            if (!bm3 && bm6 || !bm9 && bm10 || !bm13 && bm12 || !bm25 && bm18) {
                if (!bm3 && bm6) {
                    clickabletrue(m3);
                }
                if (!bm9 && bm10) {
                    clickabletrue(m9);
                }
                if (!bm13 && bm12) {
                    clickabletrue(m13);
                }
                if (!bm25 && bm18) {
                    clickabletrue(m25);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar3") && marble_drw(m3, m6)) {
                marblesChange(m3, m6, m11);
                bm3 = false;
                bm6 = false;
                bm11 = true;
            } else if (mar.equalsIgnoreCase("mar9") && marble_drw(m9, m10)) {
                marblesChange(m9, m10, m11);
                bm9 = false;
                bm10 = false;
                bm11 = true;
            } else if (mar.equalsIgnoreCase("mar13") && marble_drw(m13, m12)) {
                marblesChange(m13, m12, m11);
                bm13 = false;
                bm12 = false;
                bm11 = true;
            } else if (mar.equalsIgnoreCase("mar25") && marble_drw(m25, m18)) {
                marblesChange(m25, m18, m11);
                bm25 = false;
                bm18 = false;
                bm11 = true;
            }
            gameover();
        }
        mar = "mar11";
    }

    @OnClick(R.id.m12)
    public void m12() {
        if (!m12.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m12).setDestView(m12).startAnimation();
        if (bm12) {
            if (!bm10 && bm11 || !bm26 && bm19) {
                if (!bm10 && bm11) {
                    clickabletrue(m10);
                }
                if (!bm26 && bm19) {
                    clickabletrue(m26);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar10") && marble_drw(m10, m11)) {
                marblesChange(m10, m11, m12);
                bm10 = false;
                bm11 = false;
                bm12 = true;
            } else if (mar.equalsIgnoreCase("mar26") && marble_drw(m26, m19)) {
                marblesChange(m26, m19, m12);
                bm26 = false;
                bm19 = false;
                bm12 = true;
            }
            gameover();
        }
        mar = "mar12";
    }

    @OnClick(R.id.m13)
    public void m13() {
        if (!m13.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m13).setDestView(m13).startAnimation();
        if (bm13) {
            if (!bm11 && bm12 || !bm27 && bm20) {
                if (!bm11 && bm12) {
                    clickabletrue(m11);
                }
                if (!bm27 && bm20) {
                    clickabletrue(m27);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar11") && marble_drw(m11, m12)) {
                marblesChange(m11, m12, m13);
                bm11 = false;
                bm12 = false;
                bm13 = true;
            } else if (mar.equalsIgnoreCase("mar27") && marble_drw(m27, m20)) {
                marblesChange(m27, m20, m13);
                bm27 = false;
                bm20 = false;
                bm13 = true;
            }
            gameover();
        }
        mar = "mar13";
    }

    @OnClick(R.id.m14)
    public void m14() {
        if (!m14.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m14).setDestView(m14).startAnimation();
        if (bm14) {
            if (!bm16 && bm15) {
                clickabletrue(m16);
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar16") && marble_drw(m16, m15)) {
                marblesChange(m16, m15, m14);
                bm16 = false;
                bm15 = false;
                bm14 = true;
            }
            gameover();
        }
        mar = "mar14";
    }

    @OnClick(R.id.m15)
    public void m15() {
        if (!m15.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m15).setDestView(m15).startAnimation();
        if (bm15) {
            if (!bm17 && bm16) {
                clickabletrue(m17);
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar17") && marble_drw(m17, m16)) {
                marblesChange(m17, m16, m15);
                bm17 = false;
                bm16 = false;
                bm15 = true;
            }
            gameover();
        }
        mar = "mar15";
    }

    @OnClick(R.id.m16)
    public void m16() {
        if (!m16.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m16).setDestView(m16).startAnimation();
        if (bm16) {
            if (!bm4 && bm9 || !bm14 && bm15 || !bm28 && bm23 || !bm18 && bm17) {
                if (!bm4 && bm9) {
                    clickabletrue(m4);
                }
                if (!bm14 && bm15) {
                    clickabletrue(m14);
                }
                if (!bm28 && bm23) {
                    clickabletrue(m28);
                }
                if (!bm18 && bm17) {
                    clickabletrue(m18);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar4") && marble_drw(m4, m9)) {
                marblesChange(m4, m9, m16);
                bm4 = false;
                bm9 = false;
                bm16 = true;
            } else if (mar.equalsIgnoreCase("mar14") && marble_drw(m14, m15)) {
                marblesChange(m14, m15, m16);
                bm14 = false;
                bm15 = false;
                bm16 = true;
            } else if (mar.equalsIgnoreCase("mar28") && marble_drw(m28, m23)) {
                marblesChange(m28, m23, m16);
                bm28 = false;
                bm23 = false;
                bm16 = true;
            } else if (mar.equalsIgnoreCase("mar18") && marble_drw(m18, m17)) {
                marblesChange(m18, m17, m16);
                bm18 = false;
                bm17 = false;
                bm16 = true;
            }
            gameover();
        }
        mar = "mar16";
    }

    @OnClick(R.id.m17)
    public void m17() {
        if (!m17.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m17).setDestView(m17).startAnimation();
        if (bm17) {
            if (!bm5 && bm10 || !bm15 && bm16 || !bm29 && bm24 || !bm19 && bm18) {
                if (!bm5 && bm10) {
                    clickabletrue(m5);
                }
                if (!bm15 && bm16) {
                    clickabletrue(m15);
                }
                if (!bm29 && bm24) {
                    clickabletrue(m29);
                }
                if (!bm19 && bm18) {
                    clickabletrue(m19);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar5") && marble_drw(m5, m10)) {
                marblesChange(m5, m10, m17);
                bm5 = false;
                bm10 = false;
                bm17 = true;
            } else if (mar.equalsIgnoreCase("mar15") && marble_drw(m15, m16)) {
                marblesChange(m15, m16, m17);
                bm15 = false;
                bm16 = false;
                bm17 = true;
            } else if (mar.equalsIgnoreCase("mar29") && marble_drw(m29, m24)) {
                marblesChange(m29, m24, m17);
                bm29 = false;
                bm24 = false;
                bm17 = true;
            } else if (mar.equalsIgnoreCase("mar19") && marble_drw(m19, m18)) {
                marblesChange(m19, m18, m17);
                bm19 = false;
                bm18 = false;
                bm17 = true;
            }
            gameover();
        }
        mar = "mar17";
    }

    @OnClick(R.id.m18)
    public void m18() {
        if (!m18.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m18).setDestView(m18).startAnimation();
        if (bm18) {
            if (!bm6 && bm11 || !bm16 && bm17 || !bm30 && bm25 || !bm20 && bm19) {
                if (!bm6 && bm11) {
                    clickabletrue(m6);
                }
                if (!bm16 && bm17) {
                    clickabletrue(m16);
                }
                if (!bm30 && bm25) {
                    clickabletrue(m30);
                }
                if (!bm20 && bm19) {
                    clickabletrue(m20);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar6") && marble_drw(m6, m11)) {
                marblesChange(m6, m11, m18);
                bm6 = false;
                bm11 = false;
                bm18 = true;
            } else if (mar.equalsIgnoreCase("mar16") && marble_drw(m16, m17)) {
                marblesChange(m16, m17, m18);
                bm16 = false;
                bm17 = false;
                bm18 = true;
            } else if (mar.equalsIgnoreCase("mar30") && marble_drw(m30, m25)) {
                marblesChange(m30, m25, m18);
                bm30 = false;
                bm25 = false;
                bm18 = true;
            } else if (mar.equalsIgnoreCase("mar20") && marble_drw(m20, m19)) {
                marblesChange(m20, m19, m18);
                bm20 = false;
                bm19 = false;
                bm18 = true;
            }
            gameover();
        }
        mar = "mar18";
    }

    @OnClick(R.id.m19)
    public void m19() {
        if (!m19.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m19).setDestView(m19).startAnimation();
        if (bm19) {
            if (!bm17 && bm18) {
                clickabletrue(m17);
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar17") && marble_drw(m17, m18)) {
                marblesChange(m17, m18, m19);
                bm17 = false;
                bm18 = false;
                bm19 = true;
            }
            gameover();
        }
        mar = "mar19";
    }

    @OnClick(R.id.m20)
    public void m20() {
        if (!m20.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m20).setDestView(m20).startAnimation();
        if (bm20) {
            if (!bm18 && bm19) {
                clickabletrue(m18);
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar18") && marble_drw(m18, m19)) {
                marblesChange(m18, m19, m20);
                bm18 = false;
                bm19 = false;
                bm20 = true;
            }
            gameover();
        }
        mar = "mar20";
    }

    @OnClick(R.id.m21)
    public void m21() {
        if (!m21.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m21).setDestView(m21).startAnimation();
        if (bm21) {
            if (!bm7 && bm14 || !bm23 && bm22) {
                if (!bm7 && bm14) {
                    clickabletrue(m7);
                }
                if (!bm23 && bm22) {
                    clickabletrue(m23);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar7") && marble_drw(m7, m14)) {
                marblesChange(m7, m14, m21);
                bm7 = false;
                bm14 = false;
                bm21 = true;
            } else if (mar.equalsIgnoreCase("mar23") && marble_drw(m23, m22)) {
                marblesChange(m23, m22, m21);
                bm23 = false;
                bm22 = false;
                bm21 = true;
            }
            gameover();
        }
        mar = "mar21";
    }

    @OnClick(R.id.m22)
    public void m22() {
        if (!m22.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m22).setDestView(m22).startAnimation();
        if (bm22) {
            if (!bm8 && bm15 || !bm24 && bm23) {
                if (!bm8 && bm15) {
                    clickabletrue(m8);
                }
                if (!bm24 && bm23) {
                    clickabletrue(m24);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar8") && marble_drw(m8, m15)) {
                marblesChange(m8, m15, m22);
                bm8 = false;
                bm15 = false;
                bm22 = true;
            } else if (mar.equalsIgnoreCase("mar24") && marble_drw(m24, m23)) {
                marblesChange(m24, m23, m22);
                bm24 = false;
                bm23 = false;
                bm22 = true;
            }
            gameover();
        }
        mar = "mar22";
    }

    @OnClick(R.id.m23)
    public void m23() {
        if (!m23.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m23).setDestView(m23).startAnimation();
        if (bm23) {
            if (!bm9 && bm16 || !bm21 && bm22 || !bm31 && bm28 || !bm25 && bm24) {
                if (!bm9 && bm16) {
                    clickabletrue(m9);
                }
                if (!bm21 && bm22) {
                    clickabletrue(m21);
                }
                if (!bm31 && bm28) {
                    clickabletrue(m31);
                }
                if (!bm25 && bm24) {
                    clickabletrue(m25);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar9") && marble_drw(m9, m16)) {
                marblesChange(m9, m16, m23);
                bm9 = false;
                bm16 = false;
                bm23 = true;
            } else if (mar.equalsIgnoreCase("mar21") && marble_drw(m21, m22)) {
                marblesChange(m21, m22, m23);
                bm21 = false;
                bm22 = false;
                bm23 = true;
            } else if (mar.equalsIgnoreCase("mar31") && marble_drw(m31, m28)) {
                marblesChange(m31, m28, m23);
                bm31 = false;
                bm28 = false;
                bm23 = true;
            } else if (mar.equalsIgnoreCase("mar25") && marble_drw(m25, m24)) {
                marblesChange(m25, m24, m23);
                bm25 = false;
                bm24 = false;
                bm23 = true;
            }
            gameover();
        }
        mar = "mar23";
    }

    @OnClick(R.id.m24)
    public void m24() {
        if (!m24.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m24).setDestView(m24).startAnimation();
        if (bm24) {
            if (!bm10 && bm17 || !bm22 && bm23 || !bm32 && bm29 || !bm26 && bm25) {
                if (!bm10 && bm17) {
                    clickabletrue(m10);
                }
                if (!bm22 && bm23) {
                    clickabletrue(m22);
                }
                if (!bm32 && bm29) {
                    clickabletrue(m32);
                }
                if (!bm26 && bm25) {
                    clickabletrue(m26);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar10") && marble_drw(m10, m17)) {
                marblesChange(m10, m17, m24);
                bm10 = false;
                bm17 = false;
                bm24 = true;
            } else if (mar.equalsIgnoreCase("mar22") && marble_drw(m22, m23)) {
                marblesChange(m22, m23, m24);
                bm22 = false;
                bm23 = false;
                bm24 = true;
            } else if (mar.equalsIgnoreCase("mar32") && marble_drw(m32, m29)) {
                marblesChange(m32, m29, m24);
                bm32 = false;
                bm29 = false;
                bm24 = true;
            } else if (mar.equalsIgnoreCase("mar26") && marble_drw(m26, m25)) {
                marblesChange(m26, m25, m24);
                bm26 = false;
                bm25 = false;
                bm24 = true;
            }
            gameover();
        }
        mar = "mar24";
    }

    @OnClick(R.id.m25)
    public void m25() {
        if (!m25.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m25).setDestView(m25).startAnimation();
        if (bm25) {
            if (!bm11 && bm18 || !bm23 && bm24 || !bm33 && bm30 || !bm27 && bm26) {
                if (!bm11 && bm18) {
                    clickabletrue(m11);
                }
                if (!bm23 && bm24) {
                    clickabletrue(m23);
                }
                if (!bm33 && bm30) {
                    clickabletrue(m33);
                }
                if (!bm27 && bm26) {
                    clickabletrue(m27);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar11") && marble_drw(m11, m18)) {
                marblesChange(m11, m18, m25);
                bm11 = false;
                bm18 = false;
                bm25 = true;
            } else if (mar.equalsIgnoreCase("mar23") && marble_drw(m23, m24)) {
                marblesChange(m23, m24, m25);
                bm23 = false;
                bm24 = false;
                bm25 = true;
            } else if (mar.equalsIgnoreCase("mar33") && marble_drw(m33, m30)) {
                marblesChange(m33, m30, m25);
                bm33 = false;
                bm30 = false;
                bm25 = true;
            } else if (mar.equalsIgnoreCase("mar27") && marble_drw(m27, m26)) {
                marblesChange(m27, m26, m25);
                bm27 = false;
                bm26 = false;
                bm25 = true;
            }
            gameover();
        }
        mar = "mar25";
    }

    @OnClick(R.id.m26)
    public void m26() {
        if (!m26.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m26).setDestView(m26).startAnimation();
        if (bm26) {
            if (!bm12 && bm19 || !bm24 && bm25) {
                if (!bm12 && bm19) {
                    clickabletrue(m12);
                }
                if (!bm24 && bm25) {
                    clickabletrue(m24);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar12") && marble_drw(m12, m19)) {
                marblesChange(m12, m19, m26);
                bm12 = false;
                bm19 = false;
                bm26 = true;
            } else if (mar.equalsIgnoreCase("mar24") && marble_drw(m24, m25)) {
                marblesChange(m24, m25, m26);
                bm24 = false;
                bm25 = false;
                bm26 = true;
            }
            gameover();
        }
        mar = "mar26";
    }

    @OnClick(R.id.m27)
    public void m27() {
        if (!m27.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m27).setDestView(m27).startAnimation();
        if (bm27) {
            if (!bm13 && bm20 || !bm25 && bm26) {
                if (!bm13 && bm20) {
                    clickabletrue(m13);
                }
                if (!bm25 && bm26) {
                    clickabletrue(m25);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar13") && marble_drw(m13, m20)) {
                marblesChange(m13, m20, m27);
                bm13 = false;
                bm20 = false;
                bm27 = true;
            } else if (mar.equalsIgnoreCase("mar25") && marble_drw(m25, m26)) {
                marblesChange(m25, m26, m27);
                bm25 = false;
                bm26 = false;
                bm27 = true;
            }
            gameover();
        }
        mar = "mar27";
    }

    @OnClick(R.id.m28)
    public void m28() {
        if (!m28.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m28).setDestView(m28).startAnimation();
        if (bm28) {
            if (!bm16 && bm23 || !bm30 && bm29) {
                if (!bm16 && bm23) {
                    clickabletrue(m16);
                }
                if (!bm30 && bm29) {
                    clickabletrue(m30);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar16") && marble_drw(m16, m23)) {
                marblesChange(m16, m23, m28);
                bm16 = false;
                bm23 = false;
                bm28 = true;
            } else if (mar.equalsIgnoreCase("mar30") && marble_drw(m30, m29)) {
                marblesChange(m30, m29, m28);
                bm30 = false;
                bm29 = false;
                bm28 = true;
            }
            gameover();
        }
        mar = "mar28";
    }

    @OnClick(R.id.m29)
    public void m29() {
        if (!m29.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m29).setDestView(m29).startAnimation();
        if (bm29) {
            if (!bm17 && bm24) {
                clickabletrue(m17);
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar17") && marble_drw(m17, m24)) {
                marblesChange(m17, m24, m29);
                bm17 = false;
                bm24 = false;
                bm29 = true;
            }
            gameover();
        }
        mar = "mar29";
    }

    @OnClick(R.id.m30)
    public void m30() {
        if (!m30.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m30).setDestView(m30).startAnimation();
        if (bm30) {
            if (!bm18 && bm25 || !bm28 && bm29) {
                if (!bm18 && bm25) {
                    clickabletrue(m18);
                }
                if (!bm28 && bm29) {
                    clickabletrue(m28);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar18") && marble_drw(m18, m25)) {
                marblesChange(m18, m25, m30);
                bm18 = false;
                bm25 = false;
                bm30 = true;
            } else if (mar.equalsIgnoreCase("mar28") && marble_drw(m28, m29)) {
                marblesChange(m28, m29, m30);
                bm28 = false;
                bm29 = false;
                bm30 = true;
            }
            gameover();
        }
        mar = "mar30";
    }

    @OnClick(R.id.m31)
    public void m31() {
        if (!m31.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m31).setDestView(m31).startAnimation();
        if (bm31) {
            if (!bm23 && bm28 || !bm33 && bm32) {
                if (!bm23 && bm28) {
                    clickabletrue(m23);
                }
                if (!bm33 && bm32) {
                    clickabletrue(m33);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar23") && marble_drw(m23, m28)) {
                marblesChange(m23, m28, m31);
                bm23 = false;
                bm28 = false;
                bm31 = true;
            } else if (mar.equalsIgnoreCase("mar33") && marble_drw(m33, m32)) {
                marblesChange(m33, m32, m31);
                bm33 = false;
                bm32 = false;
                bm31 = true;
            }
            gameover();
        }
        mar = "mar31";
    }

    @OnClick(R.id.m32)
    public void m32() {
        if (!m32.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m32).setDestView(m32).startAnimation();
        if (bm32) {
            if (!bm24 && bm29) {
                clickabletrue(m24);
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar24") && marble_drw(m24, m29)) {
                marblesChange(m24, m29, m32);
                bm24 = false;
                bm29 = false;
                bm32 = true;
            }
            gameover();
        }
        mar = "mar32";
    }

    @OnClick(R.id.m33)
    public void m33() {
        if (!m33.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()))
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(m33).setDestView(m33).startAnimation();
        if (bm33) {
            if (!bm25 && bm30 || !bm31 && bm32) {
                if (!bm25 && bm30) {
                    clickabletrue(m25);
                }
                if (!bm31 && bm32) {
                    clickabletrue(m31);
                }
                selection_play();
                title_label.setText(R.string.marble);
                title_label.setTextColor(getResources().getColor(R.color.title));

            } else {
                error_sound_play();
                title_label.startAnimation(animShake);
                title_label.setText(R.string.wrong_marble);
                title_label.setTextColor(getResources().getColor(R.color.txt_red));
            }
        } else {
            discard_marblen_sound_play();
            if (mar.equalsIgnoreCase("mar25") && marble_drw(m25, m30)) {
                marblesChange(m25, m30, m33);
                bm25 = false;
                bm30 = false;
                bm33 = true;
            } else if (mar.equalsIgnoreCase("mar31") && marble_drw(m31, m32)) {
                marblesChange(m31, m32, m33);
                bm31 = false;
                bm32 = false;
                bm33 = true;
            }
            gameover();
        }
        mar = "mar33";
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder2 = new AlertDialog.Builder(Marbles.this);
        builder2.setTitle(getResources().getString(R.string.game_name));
        builder2.setMessage(getResources().getString(R.string.home_msg));
        builder2.setCancelable(true);

        builder2.setPositiveButton(
                "yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent(Marbles.this, HomeScreen.class);
                        startActivity(i);
                        showAds();
                        dialog.cancel();
                        overridePendingTransition(0, 0);
                        // overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

                    }
                });

        builder2.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder2.show();
    }

    /**
     * This method use for moveing marbles effect.
     * */
    public void marblesChange(final View black, final View black2, final View marble) {
        //  new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(marble).setDestView(marble).startAnimation();
        new MarblesAnimation().attachActivity(Marbles.this).setTargetView(black).setDestView(marble).startAnimation();
        final Drawable to_marble = black.getBackground();
        final Drawable done_mar = black2.getBackground();

        black.setBackgroundDrawable(getResources().getDrawable(R.drawable.un_selected_marbles));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                black2.setBackgroundDrawable(getResources().getDrawable(R.drawable.un_selected_marbles));
                marble.setBackground(to_marble);
                imag_id.add(done_mar);
                score = score + 1;
                title_label.setText(R.string.nice_play);
                score_label.setText("Score : " + score);
                clickablefalse(black);
                clickablefalse(black2);
                done_marbles_list();
            }
        }, 900);
       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(marble).setDestView(marble).startAnimation();
            }
        }, 200);*/
        /*new MarblesAnimation().attachActivity(Marbles.this).setTargetView(black).setDestView(marble).startAnimation();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                black.setBackgroundDrawable(getResources().getDrawable(R.drawable.black));
                black2.setBackgroundDrawable(getResources().getDrawable(R.drawable.black));
                marble.setBackground(to_marble);
                score = score + 1;
                title_label.setText(R.string.nice_play);
                score_label.setText("Score : " + score);
                clickablefalse(black);
                clickablefalse(black2);
            }
        }, 700);*/

    }

    /**
     * This method use for marbles not clickable when it blank.
     * */
    public void clickablefalse(View v) {
        v.setClickable(false);
        v.setFocusable(false);
        v.setFocusableInTouchMode(false);

    }

    /**
     * This method use for marbles clickable when it not blank.
     * */
    public void clickabletrue(View v) {
        v.setClickable(true);
        v.setFocusable(true);
        v.setFocusableInTouchMode(false);

    }

    /**
     * This method use for game over when no move available.
     * */
    public void gameover() {
        if (bm1 && (!bm3 && bm2 || !bm9 && bm4)) {

        } else if (bm2 && (!bm10 && bm5)) {

        } else if (bm3 && (!bm11 && bm6 || !bm1 && bm2)) {

        } else if (bm4 && (!bm6 && bm5 || !bm16 && bm9)) {

        } else if (bm5 && (!bm17 && bm10)) {

        } else if (bm6 && (!bm4 && bm5 || !bm18 && bm11)) {

        } else if (bm7 && (!bm9 && bm8 || !bm21 && bm14)) {

        } else if (bm8 && (!bm10 && bm9 || !bm22 && bm15)) {

        } else if (bm9 && (!bm1 && bm4 || !bm7 && bm8 || !bm11 && bm10 || !bm23 && bm16)) {

        } else if (bm10 && (!bm2 && bm5 || !bm8 && bm9 || !bm24 && bm17 || !bm12 && bm11)) {

        } else if (bm11 && (!bm3 && bm6 || !bm9 && bm10 || !bm13 && bm12 || !bm25 && bm18)) {

        } else if (bm12 && (!bm10 && bm11 || !bm26 && bm19)) {

        } else if (bm13 && (!bm11 && bm12 || !bm27 && bm20)) {

        } else if (bm14 && (!bm16 && bm15)) {

        } else if (bm15 && (!bm17 && bm16)) {

        } else if (bm16 && (!bm4 && bm9 || !bm14 && bm15 || !bm28 && bm23 || !bm18 && bm17)) {

        } else if (bm17 && (!bm5 && bm10 || !bm15 && bm16 || !bm29 && bm24 || !bm19 && bm18)) {

        } else if (bm18 && (!bm6 && bm11 || !bm16 && bm17 || !bm30 && bm25 || !bm20 && bm19)) {

        } else if (bm19 && (!bm17 && bm18)) {

        } else if (bm20 && (!bm18 && bm19)) {

        } else if (bm21 && (!bm7 && bm14 || !bm23 && bm22)) {

        } else if (bm22 && (!bm8 && bm15 || !bm24 && bm23)) {

        } else if (bm23 && (!bm9 && bm16 || !bm21 && bm22 || !bm31 && bm28 || !bm25 && bm24)) {

        } else if (bm24 && (!bm10 && bm17 || !bm22 && bm23 || !bm32 && bm29 || !bm26 && bm25)) {

        } else if (bm25 && (!bm11 && bm18 || !bm23 && bm24 || !bm33 && bm30 || !bm27 && bm26)) {

        } else if (bm26 && (!bm12 && bm19 || !bm24 && bm25)) {

        } else if (bm27 && (!bm13 && bm20 || !bm25 && bm26)) {

        } else if (bm28 && (!bm16 && bm23 || !bm30 && bm29)) {

        } else if (bm29 && (!bm17 && bm24)) {

        } else if (bm30 && (!bm18 && bm25 || !bm28 && bm29)) {

        } else if (bm31 && (!bm23 && bm28 || !bm33 && bm32)) {

        } else if (bm32 && (!bm24 && bm29)) {

        } else if (bm33 && (!bm25 && bm30 || !bm31 && bm32)) {

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    title_label.setText("Game Over.");
                    game_over_key = true;
                    int finalscore = score;
                    String f_score = String.valueOf(finalscore);
                    int extra_marbles = 32 - finalscore;
                    editor.putInt("best_score", finalscore);
                    editor.commit();
                    final Animation game_over_slide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.game_over_slide);
                    ll_gameover.setVisibility(View.VISIBLE);
                    // ll_gameover.setAnimation(slide);
                    ll_gameover.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });

                   /* restart.setClickable(false);
                    home.setClickable(false);
                    hint.setClickable(false);
                    help.setClickable(false);
                    btnsound.setClickable(false);*/

                    ll_gameover.startAnimation(game_over_slide);


                    game_over_text.setText(f_score);
                    final Animation game_over_score = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.game_over_score);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            game_over_text.setAnimation(game_over_score);
                        }
                    }, 3000);

                    title_label.setTextColor(getResources().getColor(R.color.title));
                    //game_over_text.setText("Score :" + finalscore + "\nMarbles :" + extra_marbles);
                    /*new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = getIntent();
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                            //newgame();
                            restart.setClickable(true);
                            home.setClickable(true);
                            hint.setClickable(true);
                            help.setClickable(true);
                            btnsound.setClickable(true);
                            imag_id.clear();
                        }
                    }, 6500);*/


                }
            }, 1000);


          /*  builder1.setPositiveButton(
                    "Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = getIntent();
                            startActivity(intent);
                            finish();
                            imag_id.clear();
                            done_marbles();
                            newgame();
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            builder1.show();*/
        }
    }

    /**
     * This method use for when new game start.
     * */
    public void newgame() {
        /*for (int i = 0; i < 33; i++) {
            randomMarbles.add("");
        }*/
        bm1 = bm2 = bm3 = bm4 = bm5 = bm6 = bm7 = bm8 = bm9 = bm10 = bm11 = bm12 = true;
        bm13 = bm14 = bm15 = bm16 = bm18 = bm19 = bm20 = bm21 = bm22 = bm23 = bm24 = true;
        bm25 = bm26 = bm27 = bm28 = bm29 = bm30 = bm31 = bm32 = bm33 = true;
        bm17 = false;
        mar = "";
        score = 0;
        game_over_key = false;
        ll_gameover.setVisibility(View.GONE);
        game_over_text.setText("");
        title_label.setText("Let's do it,\nAll the best.");
        score_label.setText(R.string.score);
        imag_id.clear();
        done_marbles_list();
        // done_marbles();
        int marbles_img[] = {R.drawable.marbles1, R.drawable.marbles2, R.drawable.marbles3, R.drawable.marbles4, R.drawable.marbles5, R.drawable.marbles6,
                R.drawable.marbles7, R.drawable.marbles8, R.drawable.marbles9, R.drawable.marbles10, R.drawable.marbles11, R.drawable.marbles12,
                R.drawable.marbles13, R.drawable.marbles14, R.drawable.marbles15, R.drawable.marbles16, R.drawable.marbles17, R.drawable.marbles18,
                R.drawable.marbles19, R.drawable.marbles20, R.drawable.marbles21, R.drawable.marbles22, R.drawable.marbles23, R.drawable.marbles24,
                R.drawable.marbles25, R.drawable.marbles26, R.drawable.marbles27, R.drawable.marbles28, R.drawable.marbles29, R.drawable.marbles30,
                R.drawable.marbles31, R.drawable.marbles32

        };

        int size = 32;

        List<Integer> list1 = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list1.add(i);
        }

        Random rand = new Random();
        List<Integer> integers = new ArrayList<>();
        while (list1.size() > 0) {
            int index = rand.nextInt(list1.size());
            integers.add(list1.get(index));
            System.out.println("Selected: " + list1.remove(index));
        }

        m1.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(1)]));
        m2.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(2)]));
        m3.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(3)]));
        m4.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(4)]));
        m5.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(5)]));
        m6.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(6)]));
        m7.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(7)]));
        m8.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(8)]));
        m9.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(9)]));
        m10.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(10)]));
        m11.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(11)]));
        m12.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(12)]));
        m13.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(13)]));
        m14.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(14)]));
        m15.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(15)]));
        m16.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(16)]));
        m17.setBackgroundDrawable(getResources().getDrawable(R.drawable.un_selected_marbles));
        m18.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(17)]));
        m19.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(18)]));
        m20.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(19)]));
        m21.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(20)]));
        m22.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(21)]));
        m23.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(22)]));
        m24.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(23)]));
        m25.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(24)]));
        m26.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(25)]));
        m27.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(26)]));
        m28.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(27)]));
        m29.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(28)]));
        m30.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(29)]));
        m31.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(30)]));
        m32.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(31)]));
        m33.setBackgroundDrawable(getResources().getDrawable(marbles_img[integers.get(0)]));

        clickabletrue(m1);
        clickabletrue(m2);
        clickabletrue(m3);
        clickabletrue(m4);
        clickabletrue(m5);
        clickabletrue(m6);
        clickabletrue(m7);
        clickabletrue(m8);
        clickabletrue(m9);
        clickabletrue(m10);
        clickabletrue(m11);
        clickabletrue(m12);
        clickabletrue(m13);
        clickabletrue(m14);
        clickabletrue(m15);
        clickabletrue(m16);
        clickabletrue(m17);
        clickabletrue(m18);
        clickabletrue(m19);
        clickabletrue(m20);
        clickabletrue(m21);
        clickabletrue(m22);
        clickabletrue(m23);
        clickabletrue(m24);
        clickabletrue(m25);
        clickabletrue(m26);
        clickabletrue(m27);
        clickabletrue(m28);
        clickabletrue(m29);
        clickabletrue(m30);
        clickabletrue(m31);
        clickabletrue(m32);
        clickabletrue(m33);
    }

    public void done_marbles() {
        int numViews = 31;
        for (int i = 0; i < numViews; i++) {
            // Create some quick TextViews that can be placed.

            // Set a text and center it in each view.
           /* if(i==0)
            v.setBackgroundDrawable(getResources().getDrawable(R.drawable.mar5));
            else*/
            TextView v = new TextView(this);
            //v.setBackgroundDrawable(getResources().getDrawable(R.drawable.black));
            //v.setBackgroundColor(ContextCompat.getColor(this, R.color.invisible_color));
            v.setBackgroundResource(0);
            // v.setImageResource(null);
            //v.setBackgroundDrawable(getResources().getDrawable(R.drawable.invisible));

            for (int j = 0; j <= score; j++) {
                try {
                    v.setBackground(imag_id.get(score));
                } catch (Exception e) {
                    // v.setBackgroundDrawable(getResources().getDrawable(R.drawable.un_selected_marble));
                }
            }
            v.setGravity(Gravity.CENTER);


            // Force the views to a nice size (150x100 px) that fits my display.
            // This should of course be done in a display size independent way.
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(70, 70);
            // Place all views in the center of the layout. We'll transform them
            // away from there in the code below.
            lp.gravity = Gravity.CENTER;
            // Set layout params on view.
            v.setLayoutParams(lp);

            // Calculate the angle of the current view. Adjust by 90 degrees to
            // get View 0 at the top. We need the angle in degrees and radians.
            float angleDeg = i * 360.0f / numViews - 90.0f;
            float angleRad = (float) (angleDeg * Math.PI / 180.0f);
            // Calculate the position of the view, offset from center (300 px from
            // center). Again, this should be done in a display size independent way.
            v.setTranslationX(450 * (float) Math.cos(angleRad));
            v.setTranslationY(450 * (float) Math.sin(angleRad));
            // Set the rotation of the view.
            v.setRotation(angleDeg + 90.0f);
            main.addView(v);
        }
       /* RotateAnimation rotate = new RotateAnimation(0, 180);
        rotate.setDuration(1000);
        rotate.setInterpolator(new LinearInterpolator());


        main.startAnimation(rotate);*/
    }

    /**
     * This method use for when move done and marble show in round board.
     * */
    public void done_marbles_list() {
        int numViews = 31;
        FrameLayout.LayoutParams lp = null;
        for (int i = 0; i < numViews; i++) {
            // Create some quick TextViews that can be placed.

            // Set a text and center it in each view.
           /* if(i==0)
            v.setBackgroundDrawable(getResources().getDrawable(R.drawable.mar5));
            else*/
            TextView v = new TextView(this);
            for (int j = 0; j <= score; j++) {

                try {
                    if (i < imag_id.size()) {
                        v.setBackgroundDrawable(imag_id.get(i));
                    }
                    // main.setRotation(180+score*-5);
                } catch (Exception e) {
                    e.printStackTrace();
                    // v.setBackgroundDrawable(getResources().getDrawable(R.drawable.black));
                }
            }
            //v.setBackgroundColor(ContextCompat.getColor(this, R.color.blue));
            v.setGravity(Gravity.CENTER);


            // Force the views to a nice size (150x100 px) that fits my display.
            // This should of course be done in a display size independent way.


            if (width < 500) {
                lp = new FrameLayout.LayoutParams(32, 32);
                int w_h = 48;
                int m = 3;
                marblesize(m1, w_h, m);
                marblesize(m2, w_h, m);
                marblesize(m3, w_h, m);
                marblesize(m4, w_h, m);
                marblesize(m5, w_h, m);
                marblesize(m6, w_h, m);
                marblesize(m7, w_h, m);
                marblesize(m8, w_h, m);
                marblesize(m9, w_h, m);
                marblesize(m10, w_h, m);
                marblesize(m11, w_h, m);
                marblesize(m12, w_h, m);
                marblesize(m13, w_h, m);
                marblesize(m14, w_h, m);
                marblesize(m15, w_h, m);
                marblesize(m16, w_h, m);
                marblesize(m17, w_h, m);
                marblesize(m18, w_h, m);
                marblesize(m19, w_h, m);
                marblesize(m20, w_h, m);
                marblesize(m21, w_h, m);
                marblesize(m22, w_h, m);
                marblesize(m23, w_h, m);
                marblesize(m24, w_h, m);
                marblesize(m25, w_h, m);
                marblesize(m26, w_h, m);
                marblesize(m27, w_h, m);
                marblesize(m28, w_h, m);
                marblesize(m29, w_h, m);
                marblesize(m30, w_h, m);
                marblesize(m31, w_h, m);
                marblesize(m32, w_h, m);
                marblesize(m33, w_h, m);
            } else if (width >= 500 && width < 720) {
                int w_h = 48;
                lp = new FrameLayout.LayoutParams(34, 34);
                int m = 4;
                marblesize(m1, w_h, m);
                marblesize(m2, w_h, m);
                marblesize(m3, w_h, m);
                marblesize(m4, w_h, m);
                marblesize(m5, w_h, m);
                marblesize(m6, w_h, m);
                marblesize(m7, w_h, m);
                marblesize(m8, w_h, m);
                marblesize(m9, w_h, m);
                marblesize(m10, w_h, m);
                marblesize(m11, w_h, m);
                marblesize(m12, w_h, m);
                marblesize(m13, w_h, m);
                marblesize(m14, w_h, m);
                marblesize(m15, w_h, m);
                marblesize(m16, w_h, m);
                marblesize(m17, w_h, m);
                marblesize(m18, w_h, m);
                marblesize(m19, w_h, m);
                marblesize(m20, w_h, m);
                marblesize(m21, w_h, m);
                marblesize(m22, w_h, m);
                marblesize(m23, w_h, m);
                marblesize(m24, w_h, m);
                marblesize(m25, w_h, m);
                marblesize(m26, w_h, m);
                marblesize(m27, w_h, m);
                marblesize(m28, w_h, m);
                marblesize(m29, w_h, m);
                marblesize(m30, w_h, m);
                marblesize(m31, w_h, m);
                marblesize(m32, w_h, m);
                marblesize(m33, w_h, m);
            } else if (width >= 720 && width < 800) {
                int w_h = 68;
                lp = new FrameLayout.LayoutParams(50, 50);
                int m = 5;
                marblesize(m1, w_h, m);
                marblesize(m2, w_h, m);
                marblesize(m3, w_h, m);
                marblesize(m4, w_h, m);
                marblesize(m5, w_h, m);
                marblesize(m6, w_h, m);
                marblesize(m7, w_h, m);
                marblesize(m8, w_h, m);
                marblesize(m9, w_h, m);
                marblesize(m10, w_h, m);
                marblesize(m11, w_h, m);
                marblesize(m12, w_h, m);
                marblesize(m13, w_h, m);
                marblesize(m14, w_h, m);
                marblesize(m15, w_h, m);
                marblesize(m16, w_h, m);
                marblesize(m17, w_h, m);
                marblesize(m18, w_h, m);
                marblesize(m19, w_h, m);
                marblesize(m20, w_h, m);
                marblesize(m21, w_h, m);
                marblesize(m22, w_h, m);
                marblesize(m23, w_h, m);
                marblesize(m24, w_h, m);
                marblesize(m25, w_h, m);
                marblesize(m26, w_h, m);
                marblesize(m27, w_h, m);
                marblesize(m28, w_h, m);
                marblesize(m29, w_h, m);
                marblesize(m30, w_h, m);
                marblesize(m31, w_h, m);
                marblesize(m32, w_h, m);
                marblesize(m33, w_h, m);
            } else if (width >= 800 && width < 1080) {
                lp = new FrameLayout.LayoutParams(56, 56);
                int w_h = 72;
                int m = 8;
                marblesize(m1, w_h, m);
                marblesize(m2, w_h, m);
                marblesize(m3, w_h, m);
                marblesize(m4, w_h, m);
                marblesize(m5, w_h, m);
                marblesize(m6, w_h, m);
                marblesize(m7, w_h, m);
                marblesize(m8, w_h, m);
                marblesize(m9, w_h, m);
                marblesize(m10, w_h, m);
                marblesize(m11, w_h, m);
                marblesize(m12, w_h, m);
                marblesize(m13, w_h, m);
                marblesize(m14, w_h, m);
                marblesize(m15, w_h, m);
                marblesize(m16, w_h, m);
                marblesize(m17, w_h, m);
                marblesize(m18, w_h, m);
                marblesize(m19, w_h, m);
                marblesize(m20, w_h, m);
                marblesize(m21, w_h, m);
                marblesize(m22, w_h, m);
                marblesize(m23, w_h, m);
                marblesize(m24, w_h, m);
                marblesize(m25, w_h, m);
                marblesize(m26, w_h, m);
                marblesize(m27, w_h, m);
                marblesize(m28, w_h, m);
                marblesize(m29, w_h, m);
                marblesize(m30, w_h, m);
                marblesize(m31, w_h, m);
                marblesize(m32, w_h, m);
                marblesize(m33, w_h, m);
            } else if (width >= 1080 && width < 1400) {
                lp = new FrameLayout.LayoutParams(70, 70);
                int w_h = 100;
                int m = 8;
                marblesize(m1, w_h, m);
                marblesize(m2, w_h, m);
                marblesize(m3, w_h, m);
                marblesize(m4, w_h, m);
                marblesize(m5, w_h, m);
                marblesize(m6, w_h, m);
                marblesize(m7, w_h, m);
                marblesize(m8, w_h, m);
                marblesize(m9, w_h, m);
                marblesize(m10, w_h, m);
                marblesize(m11, w_h, m);
                marblesize(m12, w_h, m);
                marblesize(m13, w_h, m);
                marblesize(m14, w_h, m);
                marblesize(m15, w_h, m);
                marblesize(m16, w_h, m);
                marblesize(m17, w_h, m);
                marblesize(m18, w_h, m);
                marblesize(m19, w_h, m);
                marblesize(m20, w_h, m);
                marblesize(m21, w_h, m);
                marblesize(m22, w_h, m);
                marblesize(m23, w_h, m);
                marblesize(m24, w_h, m);
                marblesize(m25, w_h, m);
                marblesize(m26, w_h, m);
                marblesize(m27, w_h, m);
                marblesize(m28, w_h, m);
                marblesize(m29, w_h, m);
                marblesize(m30, w_h, m);
                marblesize(m31, w_h, m);
                marblesize(m32, w_h, m);
                marblesize(m33, w_h, m);
            } else if (width >= 1400) {
                int w_h = 133;
                lp = new FrameLayout.LayoutParams(87, 87);
                int m = 12;
                marblesize(m1, w_h, m);
                marblesize(m2, w_h, m);
                marblesize(m3, w_h, m);
                marblesize(m4, w_h, m);
                marblesize(m5, w_h, m);
                marblesize(m6, w_h, m);
                marblesize(m7, w_h, m);
                marblesize(m8, w_h, m);
                marblesize(m9, w_h, m);
                marblesize(m10, w_h, m);
                marblesize(m11, w_h, m);
                marblesize(m12, w_h, m);
                marblesize(m13, w_h, m);
                marblesize(m14, w_h, m);
                marblesize(m15, w_h, m);
                marblesize(m16, w_h, m);
                marblesize(m17, w_h, m);
                marblesize(m18, w_h, m);
                marblesize(m19, w_h, m);
                marblesize(m20, w_h, m);
                marblesize(m21, w_h, m);
                marblesize(m22, w_h, m);
                marblesize(m23, w_h, m);
                marblesize(m24, w_h, m);
                marblesize(m25, w_h, m);
                marblesize(m26, w_h, m);
                marblesize(m27, w_h, m);
                marblesize(m28, w_h, m);
                marblesize(m29, w_h, m);
                marblesize(m30, w_h, m);
                marblesize(m31, w_h, m);
                marblesize(m32, w_h, m);
                marblesize(m33, w_h, m);
            } else {
                lp = new FrameLayout.LayoutParams(70, 70);
            }
            // Place all views in the center of the layout. We'll transform them
            // away from there in the code below.
            lp.gravity = Gravity.CENTER;
            // Set layout params on view.
            v.setLayoutParams(lp);

            // Calculate the angle of the current view. Adjust by 90 degrees to
            // get View 0 at the top. We need the angle in degrees and radians.
            float angleDeg = i * 360.0f / numViews - 90.0f;
            float angleRad = (float) (angleDeg * Math.PI / 180.0f);
            // Calculate the position of the view, offset from center (300 px from
            // center). Again, this should be done in a display size independent way.
            if (width < 500) {
                marbles_board.getLayoutParams().height = 480;
                marbles_board.getLayoutParams().width = 480;
                ll_gameover.getLayoutParams().height = 480;
                ll_gameover.getLayoutParams().width = 480;
                v.setTranslationX(212 * (float) Math.cos(angleRad));
                v.setTranslationY(212 * (float) Math.sin(angleRad));
            } else if (width >= 500 && width < 720) {
                marbles_board.getLayoutParams().height = 500;
                marbles_board.getLayoutParams().width = 500;
                ll_gameover.getLayoutParams().height = 500;
                ll_gameover.getLayoutParams().width = 500;
                v.setTranslationX(222 * (float) Math.cos(angleRad));
                v.setTranslationY(222 * (float) Math.sin(angleRad));
            } else if (width >= 720 && width < 800) {
                marbles_board.getLayoutParams().height = 710;
                marbles_board.getLayoutParams().width = 710;
                ll_gameover.getLayoutParams().height = 710;
                ll_gameover.getLayoutParams().width = 710;
                v.setTranslationX(315 * (float) Math.cos(angleRad));
                v.setTranslationY(315 * (float) Math.sin(angleRad));
            } else if (width >= 800 && width < 1080) {
                marbles_board.getLayoutParams().height = 790;
                marbles_board.getLayoutParams().width = 790;
                ll_gameover.getLayoutParams().height = 790;
                ll_gameover.getLayoutParams().width = 790;
                title_label.setTextSize(22);
                v.setTranslationX(350 * (float) Math.cos(angleRad));
                v.setTranslationY(350 * (float) Math.sin(angleRad));
            } else if (width >= 1080 && width < 1400) {
                marbles_board.getLayoutParams().height = 1050;
                marbles_board.getLayoutParams().width = 1050;
                ll_gameover.getLayoutParams().height = 1050;
                ll_gameover.getLayoutParams().width = 1050;
                title_label.setTextSize(25);
                v.setTranslationX(470 * (float) Math.cos(angleRad));
                v.setTranslationY(470 * (float) Math.sin(angleRad));
            } else if (width >= 1400) {
                marbles_board.getLayoutParams().height = 1380;
                marbles_board.getLayoutParams().width = 1380;
                ll_gameover.getLayoutParams().height = 1380;
                ll_gameover.getLayoutParams().width = 1380;
                title_label.setTextSize(30);
                v.setTranslationX(614 * (float) Math.cos(angleRad));
                v.setTranslationY(614 * (float) Math.sin(angleRad));
            } else {
                v.setTranslationX(420 * (float) Math.cos(angleRad));
                v.setTranslationY(420 * (float) Math.sin(angleRad));
            }
            // Set the rotation of the view.
            v.setRotation(angleDeg + 90.0f);
            main.addView(v);
        }
        /*RotateAnimation rotate = new RotateAnimation(0, 180);
        rotate.setDuration(1000);
        rotate.setInterpolator(new LinearInterpolator());


        main.startAnimation(rotate);*/
    }

    /**
     * This method use for play sound when discard marble.
     * */
    public void discard_marblen_sound_play() {
        if (pref.getBoolean("sound", false)) {
            if (discard_marble.isPlaying()) {
                discard_marble.seekTo(0);
                discard_marble.start();
            } else {
                discard_marble.start();
            }
        }
    }

    /**
     * This method use for play sound when error marble.
     * */
    public void error_sound_play() {
        vib = (Vibrator) getSystemService(getApplicationContext().VIBRATOR_SERVICE);
        vib.vibrate(350);
        if (pref.getBoolean("sound", false)) {
            if (error_sound.isPlaying()) {
                error_sound.seekTo(0);
                error_sound.start();
            } else {
                error_sound.start();
            }

        }
    }

    /**
     * This method use for play sound when select marble.
     * */
    public void selection_play() {

        if (pref.getBoolean("sound", false)) {
            if (selection.isPlaying()) {
                selection.pause();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selection.seekTo(0);
                        selection.start();
                    }
                }, 200);
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selection.start();
                    }
                }, 200);
            }
        }
    }

    /**
     * This method use for play sound when click button.
     * */
    public void btn_selection_play() {
        if (pref.getBoolean("sound", false)) {
            if (selection.isPlaying()) {
                selection.seekTo(0);
                selection.start();
            } else {
                selection.start();
            }
        }
    }

    /**
     * This method use for hint move.
     * */
    public void hint() {
        if (bm1 && (!bm3 && bm2 || !bm9 && bm4)) {
            hint_two(m1, m3, m9, bm3, bm2);
        } else if (bm2 && (!bm10 && bm5)) {
            hint_one(m2, m10);
        } else if (bm3 && (!bm11 && bm6 || !bm1 && bm2)) {
            hint_two(m3, m11, m1, bm11, bm6);
        } else if (bm4 && (!bm6 && bm5 || !bm16 && bm9)) {
            hint_two(m4, m6, m16, bm6, bm5);
        } else if (bm5 && (!bm17 && bm10)) {
            hint_one(m5, m17);
        } else if (bm6 && (!bm4 && bm5 || !bm18 && bm11)) {
            hint_two(m6, m4, m18, bm4, bm5);
        } else if (bm7 && (!bm9 && bm8 || !bm21 && bm14)) {
            hint_two(m7, m9, m21, bm9, bm8);
        } else if (bm8 && (!bm10 && bm9 || !bm22 && bm15)) {
            hint_two(m8, m10, m22, bm10, bm9);
        } else if (bm9 && (!bm1 && bm4 || !bm7 && bm8 || !bm11 && bm10 || !bm23 && bm16)) {
            hint_four(m9, m1, m7, m11, m23, bm1, bm4, bm7, bm8, bm11, bm10);
        } else if (bm10 && (!bm2 && bm5 || !bm8 && bm9 || !bm24 && bm17 || !bm12 && bm11)) {
            hint_four(m10, m2, m8, m24, m12, bm2, bm5, bm8, bm9, bm24, bm17);
        } else if (bm11 && (!bm3 && bm6 || !bm9 && bm10 || !bm13 && bm12 || !bm25 && bm18)) {
            hint_four(m11, m3, m9, m13, m25, bm3, bm6, bm9, bm10, bm13, bm12);
        } else if (bm12 && (!bm10 && bm11 || !bm26 && bm19)) {
            hint_two(m12, m10, m26, bm10, bm11);
        } else if (bm13 && (!bm11 && bm12 || !bm27 && bm20)) {
            hint_two(m13, m11, m27, bm11, bm12);
        } else if (bm14 && (!bm16 && bm15)) {
            hint_one(m14, m16);
        } else if (bm15 && (!bm17 && bm16)) {
            hint_one(m15, m17);
        } else if (bm16 && (!bm4 && bm9 || !bm14 && bm15 || !bm28 && bm23 || !bm18 && bm17)) {
            hint_four(m16, m4, m14, m28, m18, bm4, bm9, bm14, bm15, bm28, bm23);
        } else if (bm17 && (!bm5 && bm10 || !bm15 && bm16 || !bm29 && bm24 || !bm19 && bm18)) {
            hint_four(m17, m5, m15, m29, m19, bm5, bm10, bm15, bm16, bm29, bm24);
        } else if (bm18 && (!bm6 && bm11 || !bm16 && bm17 || !bm30 && bm25 || !bm20 && bm19)) {
            hint_four(m18, m6, m16, m30, m20, bm6, bm11, bm16, bm17, bm30, bm25);
        } else if (bm19 && (!bm17 && bm18)) {
            hint_one(m19, m17);
        } else if (bm20 && (!bm18 && bm19)) {
            hint_one(m20, m18);
        } else if (bm21 && (!bm7 && bm14 || !bm23 && bm22)) {
            hint_two(m21, m7, m23, bm7, bm14);
        } else if (bm22 && (!bm8 && bm15 || !bm24 && bm23)) {
            hint_two(m22, m8, m24, bm8, bm15);
        } else if (bm23 && (!bm9 && bm16 || !bm21 && bm22 || !bm31 && bm28 || !bm25 && bm24)) {
            hint_four(m23, m9, m21, m31, m25, bm9, bm16, bm21, bm22, bm31, bm28);
        } else if (bm24 && (!bm10 && bm17 || !bm22 && bm23 || !bm32 && bm29 || !bm26 && bm25)) {
            hint_four(m24, m10, m22, m32, m26, bm10, bm17, bm22, bm23, bm32, bm29);
        } else if (bm25 && (!bm11 && bm18 || !bm23 && bm24 || !bm33 && bm30 || !bm27 && bm26)) {
            hint_four(m25, m11, m23, m33, m27, bm11, bm18, bm23, bm24, bm33, bm30);
        } else if (bm26 && (!bm12 && bm19 || !bm24 && bm25)) {
            hint_two(m26, m12, m24, bm12, bm19);
        } else if (bm27 && (!bm13 && bm20 || !bm25 && bm26)) {
            hint_two(m27, m13, m25, bm13, bm20);
        } else if (bm28 && (!bm16 && bm23 || !bm30 && bm29)) {
            hint_two(m28, m16, m30, bm16, bm23);
        } else if (bm29 && (!bm17 && bm24)) {
            hint_one(m29, m17);
        } else if (bm30 && (!bm18 && bm25 || !bm28 && bm29)) {
            hint_two(m30, m18, m28, bm18, bm25);
        } else if (bm31 && (!bm23 && bm28 || !bm33 && bm32)) {
            hint_two(m31, m23, m33, bm23, bm28);
        } else if (bm32 && (!bm24 && bm29)) {
            hint_one(m32, m24);
        } else if (bm33 && (!bm25 && bm30 || !bm31 && bm32)) {
            hint_two(m33, m25, m31, bm25, bm30);
        } else {

        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showAds();
            }
        }, 1200);

    }

    /**
     * This method use in hint() when one side move available .
     * */
    public void hint_one(View to, View from) {
        new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(to).setDestView(to).startAnimation();
        new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(from).setDestView(from).startAnimation();
    }

    /**
     * This method use in hint() when two side move available .
     * */
    public void hint_two(View to, View from, View from2, boolean b1, boolean b2) {
        new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(to).setDestView(to).startAnimation();
        if (!b1 && b2)
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(from).setDestView(from).startAnimation();
        else
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(from2).setDestView(from2).startAnimation();
    }

    /**
     * This method use in hint() when four side move available .
     * */
    public void hint_four(View to, View from1, View from2, View from3, View from4, boolean b1, boolean b2, boolean b3, boolean b4, boolean b5, boolean b6) {
        new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(to).setDestView(to).startAnimation();
        if (!b1 && b2)
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(from1).setDestView(from1).startAnimation();
        else if (!b3 && b4)
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(from2).setDestView(from2).startAnimation();
        else if (!b5 && b6)
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(from3).setDestView(from3).startAnimation();
        else
            new MarblesJumpingAnimation().attachActivity(Marbles.this).setTargetView(from4).setDestView(from4).startAnimation();
    }

    /**
     * This method use for set marble size.
     * */
    public void marblesize(View v, int pix, int mar) {
        v.getLayoutParams().height = pix;
        v.getLayoutParams().width = pix;
        setMargins(v, mar, mar, mar, mar);
    }

    /**
     * This method use for button click animation.
     * */
    public void click_effect(View v) {
        AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
        v.startAnimation(buttonClick);
    }

    /**
     * This method use for set marging between marbles.
     * */
    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    /**
     * This method use for check move marble condtion .
     * */
    public boolean marble_drw(View v1, View v2) {
        if (!v1.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState()) && !v2.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.un_selected_marbles).getConstantState())) {
            return true;
        } else
            return false;
    }

    /**
     * This method use for show ads .
     * */
    public void showAds() {

        /*mInterstitialAd = new InterstitialAd(Marbles.this);

        // set the ad unit ID
        //getResources().getString(R.string.game_name)
        mInterstitialAd.setAdUnitId("ca-app-pub-3151569793657586/5276078651");
        //ca-app-pub-3151569793657586/5276078651
        String android_id = Settings.Secure.getString(getApplication().getContentResolver(), Settings.Secure.ANDROID_ID);
        String deviceId = Utils.md5(android_id).toUpperCase();
        Log.i("device id=", deviceId);
        adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice(deviceId)
                //F8729A4E287A769B5E1893B86A506C92
                .build();
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                    showInterstitial();
            }
        });*/
        if (pref.getBoolean("ads", true)) {
            Chartboost.startWithAppId(this, "59917a8ff6cd451a0ce64b0c", "d7bdd580db5f8863ec54b86d6efac9fdb8658fad");
            Chartboost.onCreate(this);
            Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
            Chartboost.showInterstitial(CBLocation.LOCATION_DEFAULT);
            Chartboost.onStart(this);
        }
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();

                    //showAds();
                    //Toast.makeText(Marbles.this, "ads_closed", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    //showAds();
                    //Toast.makeText(Marbles.this, "ads_failed_to_load", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                    //Toast.makeText(Marbles.this, "ads_left_app", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                    //Toast.makeText(Marbles.this, "ads_open", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    //Toast.makeText(Marbles.this, "ads_loaded", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }



}