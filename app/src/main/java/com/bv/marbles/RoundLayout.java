package com.bv.marbles;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;

public class RoundLayout extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_round_layout);
        final FrameLayout main = (FrameLayout)findViewById(R.id.main);

        int numViews = 30;

        for(int i = 0; i < numViews; i++)
        {
            // Create some quick TextViews that can be placed.

            // Set a text and center it in each view.
           /* if(i==0)
            v.setBackgroundDrawable(getResources().getDrawable(R.drawable.mar5));
            else*/
            TextView v = new TextView(this);
            if(i==0) {
                v.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles4));
            }else {
                v.setBackgroundDrawable(getResources().getDrawable(R.drawable.marbles5));
            }
            v.setGravity(Gravity.CENTER);



            // Force the views to a nice size (150x100 px) that fits my display.
            // This should of course be done in a display size independent way.
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(80, 80);
            // Place all views in the center of the layout. We'll transform them
            // away from there in the code below.
            lp.gravity = Gravity.CENTER;
            // Set layout params on view.
            v.setLayoutParams(lp);

            // Calculate the angle of the current view. Adjust by 90 degrees to
            // get View 0 at the top. We need the angle in degrees and radians.
            float angleDeg = i * 360.0f / numViews - 90.0f;
            float angleRad = (float)(angleDeg * Math.PI / 180.0f);
            // Calculate the position of the view, offset from center (300 px from
            // center). Again, this should be done in a display size independent way.
            v.setTranslationX(450 * (float)Math.cos(angleRad));
            v.setTranslationY(450 * (float)Math.sin(angleRad));
            // Set the rotation of the view.
            v.setRotation(angleDeg + 90.0f);
            main.addView(v);
        }
        RotateAnimation rotate = new RotateAnimation(0, 180);
        rotate.setDuration(0);
        rotate.setInterpolator(new LinearInterpolator());


        main.startAnimation(rotate);
    }
}
