package com.bv.marbles;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class HowToPlay extends AppCompatActivity {
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static final Integer[] M_Image = {R.drawable.slider1, R.drawable.slider2, R.drawable.slider5, R.drawable.slider3, R.drawable.slider4};
    private static final String[] M_hint_Image = {"Move only vertical","Move only horizontal", "Cross move not allowed", "Select 1 marble and jump to empty place", "Between selected marble and empty space there must be 1 marble"};
    private ArrayList<Integer> M_Array = new ArrayList<Integer>();
    private ArrayList<String> M_hint_Array = new ArrayList<String>();
    MediaPlayer discard_marble, error_sound, selection;
    SharedPreferences pref;
    @BindView(R.id.how_to_play_back)
    TextView how_to_play_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_play);
        ButterKnife.bind(this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        selection = MediaPlayer.create(this, R.raw.selection);

        m_slider();

    }

    /**
     * This click event use for back to play game screen
     * */
    @OnClick(R.id.how_to_play_back)
    public void back() {
        selection_play();
        onBackPressed();
        overridePendingTransition(0, 0);
        // overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }
    /**
     * This method use for view pager
     * */
    private void m_slider() {
        for (int i = 0; i < M_Image.length; i++) {
            M_Array.add(M_Image[i]);
            M_hint_Array.add(M_hint_Image[i]);
        }
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new HowToPlayAdapter(HowToPlay.this, M_Array,M_hint_Array));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

    }

    // Auto start of viewpager

  /*  final Handler handler = new Handler();
    final Runnable Update = new Runnable() {
        public void run() {
            if (currentPage == M_Image.length) {
                currentPage = 0;
            }
            mPager.setCurrentItem(currentPage++, true);
        }
    };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);
        */

    /**
     * This method use for play sound
     * */
    public void selection_play() {
        if (pref.getBoolean("sound", false)) {
            selection = MediaPlayer.create(this, R.raw.selection);
            selection.start();

        }
    }

}